Mint listaadminisztrátor, az Ön engedélyére van szükség
az alábbi levelezőlista-bejegyzéshez:

	Lista:	$listname
	Feladó:	$sender_email
	Tárgy:	$subject

Az üzenetet az alábbi ok(ok) miatt tartjuk vissza:

$reasons

Kérjük, kényelmes időpontban látogasson el a vezérlőpultjára,
hogy jóváhagyja vagy elutasítsa a kérelmet.
