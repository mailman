Þ         ô  -  Ì      "  ö  "  a  x$     Ú&     ã&     ï&     û&     '     '     2'  =   J'     '  T   
(  Ã  _(  Ó   #*    ÷*  ;    -     <-  c   Ô-  ¿   8.  ú   ø.     ó/  }   0  ç   0  '   k1  «   1  ³   ?2  Y   ó2  Ê   M3  ö   4  ¨   5  Ù   ¸5  ï  6  p   8  *   ó8  g   9  ^   9  \   å9  $  B:  Y   g;  o   Á;  s   1<  _   ¥<  G   =     M=  q  `=  â  Ò>  á  µ@  C   B     ÛB  ¯   gC     D     ¦D     0E    ½E     ØI  y   öI  ß  pJ  P   PM  I   ¡M  Ú   ëM  Û   ÆN  ã   ¢O  :   P  P   ÁP  J   Q  9   ]Q  Ó   Q  Ð   kR     <S  {   ÀS  b   <T  ^   T  R   þT  Ë   QU  í   V  g   W  >  sW     ²X  ¬   ÑX    ~Y  q   \     \  â   ¨\  I   ]  ¦   Õ]  t   |^  J   ñ^  ô   <_  o   1`  w   ¡`  ¹   a     Óa  &   âa  #   	b  <   -b  8   jb  M   £b  =   ñb  :   /c  C   jc  0   ®c  /   ßc  1   d  @   Ad  U   d     Ød  Q   ød  P   Je     e  4   µe  %   êe  %   f  G   6f  J   ~f  K   Éf  G   g  @   ]g  1   g  9   Ðg  .   
h  8   9h     rh     h  (   h  3   Äh  #   øh     i  K   ©i     õi     j  7   $j      \j      }j  3   j     Òj  (   ñj  (   k     Ck  0   Yk  .   k  '   ¹k     ák     õk     
l  <    l  É   ]l  !   'm     Im  Ñ  bm  D   4o     yo  -   o  0   Áo  *   òo  7   p  6   Up  8   p  8   Åp  )   þp  2   (q  '   [q  1   q  #   µq  *   Ùq  /   r  $   4r  D   Yr      r  	   ¿r  #   Ér  ,   ír     s  ,   +s     Xs     fs  &   ts  4   s     Ðs  3   ës     t  	   9t  /   Ct     st  $   {t  "    t  |   Ãt     @u     ]u  C   |u     Àu  +   Ûu  *   v     2v     Lv  (   lv  $   v  8   ºv  1   óv  !   %w    Gw  #   _y  m  y     ñz  "   {  #   3{  #   W{  (   {{  9   ¤{     Þ{     ù{  '   |  -   A|  '   o|     |     ³|  (   Ë|     ô|  C   }     Q}     `}  %   x}  :   }  )   Ù}     ~     ~     %~  d   E~  (   ª~  )   Ó~  "   ý~  %      *   F  "   q  )        ¾     Ñ     ð  ,         :  #   [       !     *   ¸     ã            ,   *     W     [  Q   k  A   ½  S   ÿ     S     m       .   ©     Ø  #   î           3  '   O     w          ¨  4   À     õ       2     8   P       '        Å  %   Ö  4   ü  =   1  1   o  C   ¡  %   å  +     %   7     ]     p        +   ¯  #  Û  y  ÿ     y  0     !   ·  ,   Ù          !     ;  2   Y       $   «     Ð  '   ê       2        Q     q  6         Á  %   â  8        A  .   Y        1   ©     Û  -   õ     #  1   0     b  -   w     ¥      ¾     ß  æ   ô  "   Û  "   þ     !     @     ]  }   |  w   ú  e   r    Ø     ð  )   }  6   §      Þ  1   ÿ  %   1  4   W  5     6   Â  7   ù  6   1  "   h  &     0   ²     ã  /   þ  G  .  V   v  "   Í     ð            &   <  '   c            	   ´  =   ¾    ü  2     Ò   G  L     F   g  u  ®  R   $  S   w  @   Ë  '     J   4               2     K     f     o  !   x          µ     Ç     Ú      ô       /        N     i  !        «     É     é           %   "   E   !   h            ª      É      å       ¡     %¡  "   ?¡     b¡     |¡     ¡     ³¡     Ñ¡     î¡     ¢     ¢     ¢  *   4¢     _¢  Ö  b¢  ö  9¤    0¦     D©     U©     j©     {©     ©      °©      Ñ©  S   ò©     Fª  T   Ûª  Ã  0«  E  ô¬    :®  S   C°     °  c   /±    ±  ú   £²     ³  }   ¶³  ç   4´  2   µ  «   Oµ  ³   ûµ  Y   ¯¶    	·  R  ¸  ý   h¹  M  fº    ´»  p   T¾  5   Å¾  ­   û¾  ^   ©¿  \   À  $  eÀ     Á  Á   Â  ª   ÛÂ     Ã  k   Ä     Ä  q  Ä  â  Æ  U  òÇ  e   HÊ     ®Ê  ¯   :Ë     êË     yÌ     Í  §  Í  &   8Ó  y   _Ó  ß  ÙÓ  h   ¹Ö  I   "×  Ú   l×  =  GØ  #  Ù  :   ©Ú  P   äÚ  J   5Û  9   Û  Ó   ºÛ    Ü     ­Ý  {   HÞ  b   ÄÞ  ^   'ß  R   ß  Ë   Ùß  í   ¥à  g   á  >  ûá  (   :ã  ¬   cã  É  ä  q   Úç     Lè  â   eè  O   Hé  ¦   é  Î   ?ê  |   ë  k  ë  o   ÷ì  w   gí  ¹   ßí     î  ;   ®î  .   êî  7   ï  5   Qï  h   ï  P   ðï  ?   Að  V   ð  J   Øð  2   #ñ  E   Vñ  Q   ñ  X   îñ  /   Gò  Q   wò  ]   Éò  $   'ó  @   Ló  6   ó  (   Äó  K   íó  Z   9ô  I   ô  \   Þô  F   ;õ  >   õ  O   Áõ  9   ö  B   Kö     ö  '    ö  .   Èö  3   ÷ö  ,   +÷  ©   X÷  |   ø     ø     ø  ]   °ø  9   ù  +   Hù  J   tù  (   ¿ù  :   èù  :   #ú      ^ú  H   ú  @   Èú  /   	û  $   9û  +   ^û  (   û  <   ³û  É   ðû  9   ºü     ôü  Ñ  ý  b   ãþ  "   Fÿ  2   iÿ  ;   ÿ  @   Øÿ  O     f   i  H   Ð  Q    /   k _    7   û I   3 7   } =   µ <   ó <   0 o   m )   Ý     )    8   A    z B    "   Î "   ñ 2    S   G     K   ¶ ,       / ;   >    z /    T   · ´    '   Á    é M       S ;   q =   ­    ë /   	 @   5	 $   v	 D   	 i   à	 4   J
   
 0      È (   ã =    3   J 3   ~ -   ² 9   à     *   8 (   c E    '   Ò *   ú ,   % >   R /    r   Á    4    H 0   ` J    @   Ü     !   , 1   N     A    K   S R    7   ò /   * '   Z ,       ¯ 9   Å *   ÿ ?   * #   j 0       ¿ 2   Ü >    '   N    v %    D   ²    ÷     Y    \   o n   Ì 2   ; %   n 6    A   Ë     *   ( +   S ,    ;   ¬ $   è '       5 >   T )        ½ O   Þ N   .    } ;       Í B   å {   ( h   ¤ L    C   Z 9    ?   Ø 0       I    g      B   ¦   é y  n!    è" 0   ÷" )   (# ;   R# !   # "   °# /   Ó# E   $ 1   I$ $   {$ +    $ ,   Ì$    ù$ 2   % 0   :% ,   k% K   % :   ä% )   & 8   I& /   & .   ²& +   á& U   ' #   c' e   '    í' 3   û'    /( F   C( $   ( /   ¯( "   ß(   ) @   * 3   I* '   }* 8   ¥* '   Þ*    + ¨   ¤+    M,   Î, ¾   j. -   )/ Z   W/ +   ²/ B   Þ/ 2   !0 9   T0 >   0 <   Í0 A   
1 G   L1 -   1 0   Â1 0   ó1 %   $2 =   J2 G  2 V   Ð3 +   '4    S4 (   i4 &   4 7   ¹4 4   ñ4    &5    85    W5 T   g5 ^  ¼5 <   7 Ò   X7 Q   +8 L   }8 '  Ê8 f   ò: h   Y; O   Â; ;   < N   N< Ä   <    b=    {=    =    ¯=    ¿= -   Ï=     ý=    >    9>     T> ò  u> ª  h@ /   D u  CD    ¹E    SF #  óF ¥   H    ½H n   KI O   ºI =  
J Q   HK ÿ  K á   M Ú  |N 5  WQ 1  T   ¿W   @Y ~   Q[ Ç  Ð[ N  ^ Å   ç_ Õ  ­` à  b    de    re    e d   e    üe        q  q   ¾   B  a             Ü                   £   Ï   ø   u  6  x       Û   ó   C      ý        [   é       >      V          j   ¸   Q     T  )   .   F   Ý   ã       .  ¨   ë   w         Ê     L   \   Ô              g  a    K                  µ          4   ¶   y         ;                b            M   @   Æ          d  Z  H   ¬     õ   `      ^   *           c   ò       ¦   à   ÷              %            ù       s                I      |  6   5    ñ   °   L      *  þ           +   É   ~  e  ­                 O                  Ð   »           0   Ú           i  «             -   =      Y          8  R   k   d   r   %        |   T       X   5   N      ´           ½       G  '             
      ï   D  `      2  â   ~       w           º   ^  h       È   k     <   ³             ¹       Å          b                      7             /    ?               o  !  {  ª   h  e       #  Q   }                 A  l          ]       	   z   j         ¯   H    K     o   Ç           ©            $   ®          Ó   !   m   n   Â   ÿ       C   Ë      Á      ]  =   A                ß      ê             ç       p  û   [  {          E  8       V     M  /       v  W   Ù      Ì     0        ,  ,   
   i   Ò          Y  ¡       c  u              9  @     Z   z          E   9          l           s  :   I          &       G   p                 ¼   t             #             X  á               è   4  (   }  J      )                      3  "   ð   &  J   ä   y   $  B   -     ì      _   ô   	                  P              F         ¤             ;      "      S   ×   Î   2   ¿   N     æ   x      3           Í           ö      Ä             U  <  î         1     Ñ   g   §   S  r  Þ   Ö   1              ú   7       Õ   À         ¢       f      ±   _  ²   (    n     ?  :  P               m  ü   O  U   >     \      v   +              Ã          ·   Ø     D                 å   t  f  í   '     R                ¥              W       
    Run a script.  The argument is the module path to a callable.  This
    callable will be imported and then, if --listspec/-l is also given, is
    called with the mailing list as the first argument.  If additional
    arguments are given at the end of the command line, they are passed as
    subsequent positional arguments to the callable.  For additional help, see
    --details.

    If no --listspec/-l argument is given, the script function being called is
    called with no arguments.
     
    Start the named runner, which must be one of the strings returned by the -l
    option.

    For runners that manage a queue directory, optional `slice:range` if given
    is used to assign multiple runner processes to that queue.  range is the
    total number of runners for the queue while slice is the number of this
    runner from [0..range).  For runners that do not manage a queue, slice and
    range are ignored.

    When using the `slice:range` form, you must ensure that each runner for the
    queue is given the same range value.  If `slice:runner` is not given, then
    1:1 is used.
     
- Done. 
- Ignored: 
- Results: 
- Unprocessed: 
Held Messages:
 
Held Subscriptions:
 
Held Unsubscriptions:
 
Replaced multipart/alternative part with first alternative.
 
___________________________________________
Mailman's content filtering has removed the
following MIME parts from this message.
     A more verbose output including the file system paths that Mailman is
    using.     A specification of the mailing list to operate on.  This may be the posting
    address of the list, or its List-ID.  The argument can also be a Python
    regular expression, in which case it is matched against both the posting
    address and List-ID of all mailing lists.  To use a regular expression,
    LISTSPEC must start with a ^ (and the matching is done with re.match().
    LISTSPEC cannot be a regular expression unless --run is given.     Add all member addresses in FILENAME with delivery mode as specified
    with -d/--delivery.  FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
         Add and delete members as necessary to syncronize a list's membership
    with an input file.  FILENAME is the file containing the new membership,
    one member per line.  Blank lines and lines that start with a '#' are
    ignored.  Addresses in FILENAME which are not current list members
    will be added to the list with delivery mode as specified with
    -d/--delivery.  List members whose addresses are not in FILENAME will
    be removed from the list.  FILENAME can be '-' to indicate standard input.
         Add and/or delete owners and moderators of a list.
         Additional metadata key/value pairs to add to the message metadata
    dictionary.  Use the format key=value.  Multiple -m options are
    allowed.     Change a user's email address from old_address to possibly case-preserved
    new_address.
         Configuration file to use.  If not given, the environment variable
    MAILMAN_CONFIG_FILE is consulted and used if set.  If neither are given, a
    default configuration file is loaded.     Create a mailing list.

    The 'fully qualified list name', i.e. the posting address of the mailing
    list is required.  It must be a valid email address and the domain must be
    registered with Mailman.  List names are forced to lower case.     Date: ${date}     Delete all the members of the list.  If specified, none of -f/--file,
    -m/--member or --fromall may be specified.
         Delete list members whose addresses are in FILENAME in addition to those
    specified with -m/--member if any.  FILENAME can be '-' to indicate
    standard input.  Blank lines and lines that start with a '#' are ignored.
         Delete members from a mailing list.     Delete the list member whose address is ADDRESS in addition to those
    specified with -f/--file if any.  This option may be repeated for
    multiple addresses.
         Delete the member(s) specified by -m/--member and/or -f/--file from all
    lists in the installation.  This may not be specified together with
    -a/--all or -l/--list.
         Discard all shunted messages instead of moving them back to their original
    queue.     Display a mailing list's members.
    Filtering along various criteria can be done when displaying.
    With no options given, displaying mailing list members
    to stdout is the default mode.
         Display all memberships for a user or users with address matching a
    pattern. Because part of the process involves converting the pattern
    to a SQL query with wildcards, the pattern should be simple. A simple
    string works best.
         Display only digest members of kind.
    'any' means any digest type, 'plaintext' means only plain text (rfc 1153)
    type digests, 'mime' means MIME type digests.     Display only members with a given ROLE.
    The role may be 'any', 'member', 'nonmember', 'owner', 'moderator',
    or 'administrator' (i.e. owners and moderators).
    If not given, then 'member' role is assumed.     Display only members with a given delivery status.
    'enabled' means all members whose delivery is enabled, 'any' means
    members whose delivery is disabled for any reason, 'byuser' means
    that the member disabled their own delivery, 'bybounces' means that
    delivery was disabled by the automated bounce processor,
    'byadmin' means delivery was disabled by the list
    administrator or moderator, and 'unknown' means that delivery was disabled
    for unknown (legacy) reasons.     Display only memberships with the given role.  If not given, 'all' role,
    i.e. all roles, is the default.     Display only regular delivery members.     Display output to FILENAME instead of stdout.  FILENAME
    can be '-' to indicate standard output.     Don't actually do anything, but in conjunction with --verbose, show what
    would happen.     Don't actually make the changes.  Instead, print out what would be
    done to the list.     Don't attempt to pretty print the object.  This is useful if there is some
    problem with the object and you just want to get an unpickled
    representation.  Useful with 'mailman qfile -i <file>'.  In that case, the
    list of unpickled objects will be left in a variable called 'm'.     Don't print status messages.  Error messages are still printed to standard
    error.     Don't restart the runners when they exit because of an error or a SIGUSR1.
    Use this only for debugging.     Email address of the user to be removed from the given role.
    May be repeated to delete multiple users.
         File to send the output to.  If not given, or if '-' is given, standard
    output is used.     File to send the output to.  If not given, standard output is used.     From: ${from_}     Generate the MTA alias files upon startup. Some MTA, like postfix, can't
    deliver email if alias files mentioned in its configuration are not
    present. In some situations, this could lead to a deadlock at the first
    start of mailman3 server. Setting this option to true will make this
    script create the files and thus allow the MTA to operate smoothly.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option, the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option,the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     If you are sure you want to run as root, specify --run-as-root.     Import Mailman 2.1 list data.  Requires the fully-qualified name of the
    list to import and the path to the Mailman 2.1 pickle file.     Increment the digest volume number and reset the digest number to one.  If
    given with --send, the volume number is incremented before any current
    digests are sent.     Key to use for the lookup.  If no section is given, all the key-values pair
    from any section matching the given key will be displayed.     Leaves you at an interactive prompt after all other processing is complete.
    This is the default unless the --run option is given.     List only those mailing lists hosted on the given domain, which
    must be the email host name.  Multiple -d options may be given.
         Master subprocess watcher.

    Start and watch the configured runners, ensuring that they stay alive and
    kicking.  Each runner is forked and exec'd in turn, with the master waiting
    on their process ids.  When it detects a child runner has exited, it may
    restart it.

    The runners respond to SIGINT, SIGTERM, SIGUSR1 and SIGHUP.  SIGINT,
    SIGTERM and SIGUSR1 all cause a runner to exit cleanly.  The master will
    restart runners that have exited due to a SIGUSR1 or some kind of other
    exit condition (say because of an uncaught exception).  SIGHUP causes the
    master and the runners to close their log files, and reopen then upon the
    next printed message.

    The master also responds to SIGINT, SIGTERM, SIGUSR1 and SIGHUP, which it
    simply passes on to the runners.  Note that the master will close and
    reopen its own log files on receipt of a SIGHUP.  The master also leaves
    its own process id in the file specified in the configuration file but you
    normally don't need to use this PID directly.     Message-ID: ${message_id}     Name of file containing the message to inject.  If not given, or
    '-' (without the quotes) standard input is used.     Normally, this script will refuse to run if the user id and group id are
    not set to the 'mailman' user and group (as defined when you configured
    Mailman).  If run as root, this script will change to this user and group
    before the check is made.

    This can be inconvenient for testing and debugging purposes, so the -u flag
    means that the step that sets and checks the uid/gid is skipped, and the
    program is run as the current user and group.  This flag is not recommended
    for normal production environments.

    Note though, that if you run with -u and are not in the mailman group, you
    may have permission problems, such as being unable to delete a list's
    archives through the web.  Tough luck!     Notify the list owner by email that their mailing list has been
    created.     Operate on a mailing list.

    For detailed help, see --details
         Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the digests for all mailing lists.     Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the requests for all mailing lists.     Override the default set of runners that the master will invoke, which is
    typically defined in the configuration file.  Multiple -r options may be
    given.  The values for -r are passed straight through to bin/runner.     Override the list's setting for admin_notify_mchanges.     Override the list's setting for send_goodbye_message to
    deleted members.     Override the list's setting for send_welcome_message to added members.     Override the list's setting for send_welcome_message.     Register the mailing list's domain if not yet registered.  This is
    the default behavior, but these options are provided for backward
    compatibility.  With -D do not register the mailing list's domain.     Run the named runner exactly once through its main loop.  Otherwise, the
    runner runs indefinitely until the process receives a signal.  This is not
    compatible with runners that cannot be run once.     Running mailman commands as root is not recommended and mailman will
    refuse to run as root unless this option is specified.     Section to use for the lookup.  If no key is given, all the key-value pairs
    of the given section will be displayed.     Send any collected digests for the List only if their digest_send_periodic
    is set to True.     Send any collected digests right now, even if the size threshold has not
    yet been met.     Send the added members an invitation rather than immediately adding them.
         Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.     Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.  Ignored for invited
    members.     Set the list's preferred language to CODE, which must be a registered two
    letter language code.     Specify a list owner email address.  If the address is not currently
    registered with Mailman, the address is registered and linked to a user.
    Mailman will send a confirmation message to the address, but it will also
    send a list creation notice to the address.  More than one owner can be
    specified.     Specify a list style name.     Specify the encoding of strings in PICKLE_FILE if not utf-8 or a subset
    thereof. This will normally be the Mailman 2.1 charset of the list's
    preferred_language.     Start a runner.

    The runner named on the command line is started, and it can either run
    through its main loop once (for those runners that support this) or
    continuously.  The latter is how the master runner starts all its
    subprocesses.

    -r is required unless -l or -h is given, and its argument must be one of
    the names displayed by the -l switch.

    Normally, this script should be started from `mailman start`.  Running it
    separately or with -o is generally useful only for debugging.  When run
    this way, the environment variable $MAILMAN_UNDER_MASTER_CONTROL will be
    set which subtly changes some error handling behavior.
         Start an interactive Python session, with a variable called 'm'
    containing the list of unpickled objects.     Subject: ${subject}     The gatenews command is run periodically by the nntp runner.
    If you are running it via cron, you should remove it from the crontab.
    If you want to run it manually, set _MAILMAN_GATENEWS_NNTP in the
    environment.     The list to operate on.  Required unless --fromall is specified.
         The name of the queue to inject the message to.  QUEUE must be one of the
    directories inside the queue directory.  If omitted, the incoming queue is
    used.     The role to add/delete. This may be 'owner' or 'moderator'.
    If not given, then 'owner' role is assumed.
         This option has no effect. It exists for backwards compatibility only.     User to add with the given role. This may be an email address or, if
    quoted, any display name and email address parseable by
    email.utils.parseaddr. E.g., 'Jane Doe <jane@example.com>'. May be repeated
    to add multiple users.
         [MODE] Add all member addresses in FILENAME.  This option is removed.
    Use 'mailman addmembers' instead.     [MODE] Delete all member addresses found in FILENAME.
    This option is removed. Use 'mailman delmembers' instead.     [MODE] Synchronize all member addresses of the specified mailing list
    with the member addresses found in FILENAME.
    This option is removed. Use 'mailman syncmembers' instead.  (Digest mode) ${count} matching mailing lists found: ${display_name} post acknowledgment ${email} is already a ${role.name} of ${mlist.fqdn_listname} ${email} is not a ${role.name} of ${mlist.fqdn_listname} ${member} unsubscribed from ${mlist.display_name} mailing list due to bounces ${member}'s bounce score incremented on ${mlist.display_name} ${member}'s subscription disabled on ${mlist.display_name} ${mlist.display_name} Digest, Vol ${volume}, Issue ${digest_number} ${mlist.display_name} mailing list probe message ${mlist.display_name} subscription notification ${mlist.display_name} unsubscription notification ${mlist.fqdn_listname} post from ${msg.sender} requires approval ${mlist.list_id} bumped to volume ${mlist.volume}, number ${mlist.next_digest_number} ${mlist.list_id} has no members ${mlist.list_id} is at volume ${mlist.volume}, number ${mlist.next_digest_number} ${mlist.list_id} sent volume ${mlist.volume}, number ${mlist.next_digest_number} ${name} runs ${classname} ${person} has a pending subscription for ${listname} ${person} left ${mlist.fqdn_listname} ${realname} via ${mlist.display_name} ${self.email} is already a member of mailing list ${self.fqdn_listname} ${self.email} is already a moderator of mailing list ${self.fqdn_listname} ${self.email} is already a non-member of mailing list ${self.fqdn_listname} ${self.email} is already an owner of mailing list ${self.fqdn_listname} ${self.name}: ${email} is not a member of ${mlist.fqdn_listname} ${self.name}: No valid address found to subscribe ${self.name}: No valid email address found to unsubscribe ${self.name}: no such command: ${command_name} ${self.name}: too many arguments: ${printable_arguments} (no subject) - Original message details: -------------- next part --------------
 --send and --periodic flags cannot be used together <----- start object ${count} -----> A message part incompatible with plain text digests has been removed ...
Name: ${filename}
Type: ${ctype}
Size: ${size} bytes
Desc: ${desc}
 A previous run of GNU Mailman did not exit cleanly ({}).  Try using --force A rule which always matches. Accept a message. Add a list-specific prefix to the Subject header value. Add the RFC 2369 List-* headers. Add the message to the archives. Add the message to the digest, possibly sending it. Address changed from {} to {}. Address {} already exists; can't change. Address {} is not a valid email address. Address {} not found. Addresses are not different.  Nothing to change. After content filtering, the message was empty Already subscribed (skipping): ${email} An alias for 'end'. An alias for 'join'. An alias for 'leave'. An alternative directory to output the various MTA files to. And you can print the list's request address by running:

% mailman withlist -r listaddr.requestaddr -l ant@example.com
Importing listaddr ...
Running listaddr.requestaddr() ...
ant-request@example.com Announce only mailing list style. Apply DMARC mitigations. As another example, say you wanted to change the display name for a particular
mailing list.  You could put the following function in a file called
`change.py`:

def change(mlist, display_name):
    mlist.display_name = display_name

and run this from the command line:

% mailman withlist -r change -l ant@example.com 'My List'

Note that you do not have to explicitly commit any database transactions, as
Mailman will do this for you (assuming no errors occured). Auto-response for your message to the "${display_name}" mailing list Bad runner spec: ${value} Calculate the owner and moderator recipients. Calculate the regular recipients of the message. Cannot import runner module: ${class_path} Cannot parse as valid email address (skipping): ${line} Cannot unshunt message ${filebase}, skipping:
${error} Catch messages that are bigger than a specified maximum. Catch messages with digest Subject or boilerplate quote. Catch messages with implicit destination. Catch messages with no, or empty, Subject headers. Catch messages with suspicious headers. Catch messages with too many explicit recipients. Catch mis-addressed email commands. Cleanse certain headers from all messages. Confirm a subscription or held message request. Confirmation email sent to ${person} Confirmation email sent to ${person} to leave ${mlist.fqdn_listname} Confirmation token did not match Confirmed Content filter message notification Created mailing list: ${mlist.fqdn_listname} DMARC moderation Decorate a message with headers and footers. Digest Footer Digest Header Discard a message and stop processing. Discussion mailing list style with private archives. Display Mailman's version. Display more debugging information to the log file. Echo back your arguments. Email: {} Emergency moderation is in effect for this list End of  Filter the MIME content of messages. Find DMARC policy of From: domain. For unknown reasons, the master lock could not be acquired.

Lock file: ${config.LOCK_FILE}
Lock host: ${hostname}

Exiting. Forward of moderated message GNU Mailman is already running GNU Mailman is in an unexpected state (${hostname} != ${fqdn_name}) GNU Mailman is not running GNU Mailman is running (master pid: ${pid}) GNU Mailman is stopped (stale pid: ${pid}) Generating MTA alias maps Get a list of the list members. Get help about available email commands. Get information out of a queue file. Get the normal delivery recipients from an include file. Header "{}" matched a bounce_matching_header line Header "{}" matched a header rule Here's an example of how to use the --run option.  Say you have a file in the
Mailman installation directory called 'listaddr.py', with the following two
functions:

def listaddr(mlist):
    print(mlist.posting_address)

def requestaddr(mlist):
    print(mlist.request_address)

Run methods take at least one argument, the mailing list object to operate
on.  Any additional arguments given on the command line are passed as
positional arguments to the callable.

If -l is not given then you can run a function that takes no arguments.
 Hold a message and stop processing. If you reply to this message, keeping the Subject: header intact, Mailman will
discard the held message.  Do this if the message is spam.  If you reply to
this message and include an Approved: header with the list password in it, the
message will be approved for posting to the list.  The Approved: header can
also appear in the first line of the body of the reply. Ignoring non-dictionary: {0!r} Ignoring non-text/plain MIME parts Illegal list name: ${fqdn_listname} Illegal owner addresses: ${invalid} Information about this Mailman instance. Inject a message from a file into a mailing list's queue. Invalid Approved: password Invalid email address: ${email} Invalid language code: ${language_code} Invalid or unverified email address: ${email} Invalid value for [shell]use_python: {} Is the master even running? Join this mailing list. Last autoresponse notification for today Leave this mailing list. Leave this mailing list.

You may be asked to confirm your request. Less verbosity List all mailing lists. List already exists: ${fqdn_listname} List only those mailing lists that are publicly advertised List the available runner names and exit. List: {} Look for a posting loop. Look for any previous rule hit. Match all messages posted to a mailing list that gateways to a
        moderated newsgroup.
         Match messages sent by banned addresses. Match messages sent by moderated members. Match messages sent by nonmembers. Match messages with no valid senders. Member not subscribed (skipping): ${email} Members of the {} mailing list:
{} Membership is banned (skipping): ${email} Message ${message} Message contains administrivia Message has a digest subject Message has already been posted to this list Message has implicit destination Message has more than {} recipients Message has no subject Message quotes digest boilerplate Message sender {} is banned from this list Missing data for request {}

 Moderation chain Modify message headers. Move the message to the outgoing news queue. N/A Name: ${fname}
 New subscription request to ${self.mlist.display_name} from ${self.address.email} New unsubscription request from ${mlist.display_name} by ${email} New unsubscription request to ${self.mlist.display_name} from ${self.address.email} No child with pid: ${pid} No confirmation token found No matching mailing lists found No registered user for email address: ${email} No runner name given. No sender was found in the message. No such command: ${command_name} No such list found: ${spec} No such list matching spec: ${listspec} No such list: ${_list} No such list: ${listspec} No such queue: ${queue} Not a Mailman 2.1 configuration file: ${pickle_file} Nothing to add or delete. Nothing to do Notify list owners/moderators of pending requests. Number of objects found (see the variable 'm'): ${count} Operate on digests. Ordinary discussion mailing list style. Original Message PID unreadable in: ${config.PID_FILE} Perform ARC auth checks and attach resulting headers Perform auth checks and attach Authentication-Results header. Perform some bookkeeping after a successful post. Poll the NNTP server for messages to be gatewayed to mailing lists. Post to a moderated newsgroup gateway Posting of your message titled "${subject}" Print detailed instructions and exit. Print less output. Print some additional status. Print the Mailman configuration. Process DMARC reject or discard mitigations Produces a list of member names and email addresses.

The optional delivery= and mode= arguments can be used to limit the report
to those members with matching delivery status and/or delivery mode.  If
either delivery= or mode= is specified more than once, only the last occurrence
is used.
 Programmatically, you can write a function to operate on a mailing list, and
this script will take care of the housekeeping (see below for examples).  In
that case, the general usage syntax is:

% mailman withlist [options] -l listspec [args ...]

where `listspec` is either the posting address of the mailing list
(e.g. ant@example.com), or the List-ID (e.g. ant.example.com). Reason: {}

 Regenerate the aliases appropriate for your MTA. Regular expression requires --run Reject/bounce a message and stop processing. Remove DomainKeys headers. Removed list: ${listspec} Reopening the Mailman runners Request to mailing list "${display_name}" rejected Restarting the Mailman runners Send an acknowledgment of a posting. Send automatic responses. Send the message to the outgoing queue. Sender: {}
 Show a list of all available queue names and exit. Show also the list descriptions Show also the list names Show the current running status of the Mailman system. Show this help message and exit. Shutting down Mailman's master runner Signal the Mailman processes to re-open their log files. Stale pid file removed. Start the Mailman master and runner processes. Starting Mailman's master runner Stop and restart the Mailman runner subprocesses. Stop processing commands. Stop the Mailman master and runner processes. Subject: {}
 Subscription already pending (skipping): ${email} Subscription request Suppress some duplicates of the same message. Suppress status messages Tag messages with topic matches. The Mailman Replybot The attached message matched the ${mlist.display_name} mailing list's content
filtering rules and was prevented from being forwarded on to the list
membership.  You are receiving the only remaining copy of the discarded
message.

 The built-in -owner posting chain. The built-in header matching chain The built-in moderation chain. The built-in owner pipeline. The built-in posting pipeline. The content of this message was lost. It was probably cross-posted to
multiple lists and previously handled on another list.
 The mailing list is in emergency hold and this message was not
        pre-approved by the list administrator.
         The master lock could not be acquired because it appears as though another
master is already running. The master lock could not be acquired, because it appears as if some process
on some other host may have acquired it.  We can't test for stale locks across
host boundaries, so you'll have to clean this up manually.

Lock file: ${config.LOCK_FILE}
Lock host: ${hostname}

Exiting. The master lock could not be acquired.  It appears as though there is a stale
master lock.  Try re-running ${program} with the --force flag. The message comes from a moderated member The message has a matching Approve or Approved header. The message has no valid senders The message is larger than the {} KB maximum size The message is not from a list member The message's content type was explicitly disallowed The message's content type was not explicitly allowed The message's file extension was explicitly disallowed The message's file extension was not explicitly allowed The results of your email command are provided below.
 The results of your email commands The sender is in the nonmember {} list The variable 'm' is the ${listspec} mailing list The virgin queue pipeline. The {} list has {} moderation requests waiting. There are two ways to use this script: interactively or programmatically.
Using it interactively allows you to play with, examine and modify a mailing
list from Python's interactive interpreter.  When running interactively, the
variable 'm' will be available in the global namespace.  It will reference the
mailing list object. This script provides you with a general framework for interacting with a
mailing list. Today's Topics (${count} messages) Today's Topics:
 Undefined domain: ${domain} Undefined runner name: ${name} Unknown list style name: ${style_name} Unrecognized or invalid argument(s):
{} Unshunt messages. Unsubscription request User: {}
 Welcome to the "${mlist.display_name}" mailing list${digmode} You are not allowed to post to this mailing list From: a domain which publishes a DMARC policy of reject or quarantine, and your message has been automatically rejected.  If you think that your messages are being rejected in error, contact the mailing list owner at ${listowner}. You are not authorized to see the membership list. You can print the list's posting address by running the following from the
command line:

% mailman withlist -r listaddr -l ant@example.com
Importing listaddr ...
Running listaddr.listaddr() ...
ant@example.com You have been invited to join the ${event.mlist.fqdn_listname} mailing list. You have been unsubscribed from the ${mlist.display_name} mailing list You will be asked to confirm your subscription request and you may be issued a
provisional password.

By using the 'digest' option, you can specify whether you want digest delivery
or not.  If not specified, the mailing list's default delivery mode will be
used.  You can use the 'address' option to request subscription of an address
other than the sender of the command.
 Your confirmation is needed to join the ${event.mlist.fqdn_listname} mailing list. Your confirmation is needed to leave the ${event.mlist.fqdn_listname} mailing list. Your message to ${mlist.fqdn_listname} awaits moderator approval Your new mailing list: ${fqdn_listname} Your subscription for ${mlist.display_name} mailing list has been disabled Your urgent message to the ${mlist.display_name} mailing list was not
authorized for delivery.  The original message as received by Mailman is
attached.
 [${mlist.display_name}]  [----- end pickle -----] [----- start pickle -----] [ADD] %s [DEL] %s [No bounce details are available] [No details are available] [No reason given] [No reasons given] bad argument: ${argument} domain:admin:notice:new-list.txt help.txt ipython is not available, set use_ipython to no list:admin:action:post.txt list:admin:action:subscribe.txt list:admin:action:unsubscribe.txt list:admin:notice:disable.txt list:admin:notice:increment.txt list:admin:notice:pending.txt list:admin:notice:removal.txt list:admin:notice:subscribe.txt list:admin:notice:unrecognized.txt list:admin:notice:unsubscribe.txt list:member:digest:masthead.txt list:member:generic:footer.txt list:user:action:invite.txt list:user:action:subscribe.txt list:user:action:unsubscribe.txt list:user:notice:hold.txt list:user:notice:no-more-today.txt list:user:notice:post.txt list:user:notice:probe.txt list:user:notice:refuse.txt list:user:notice:rejected.txt list:user:notice:warning.txt list:user:notice:welcome.txt n/a not available readline not available slice and range must be integers: ${value} {} Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-09-28 13:16+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>
Language-Team: Hebrew <https://hosted.weblate.org/projects/gnu-mailman/mailman/he/>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && n % 10 == 0) ? 2 : 3));
X-Generator: Weblate 5.8-dev
 
    Run a script.  The argument is the module path to a callable.  This
    callable will be imported and then, if --listspec/-l is also given, is
    called with the mailing list as the first argument.  If additional
    arguments are given at the end of the command line, they are passed as
    subsequent positional arguments to the callable.  For additional help, see
    --details.

    If no --listspec/-l argument is given, the script function being called is
    called with no arguments.
     
    ××¤×¢××ª ×××¦× ×©×¦××× ×©×××× ×××××ª ×××ª ×××××¨××××ª ×©×××××¨× ×¢× ××× ×××¤×©×¨××ª â-l.

    ×××¦× ×× ×©×× ×××× ×ª××§×××ª ×ª××¨××, ××© `×××ª××:××××` ×× ×× ×©×¡××¤×§ ××©××© ×××§×¦××ª
    ××¡×¤×¨ ×ª××××× ××¦× ×× ××ª××¨ ×××.  ××××× ××× ××¡×¤×¨ ×××¦× ×× ××××× ××ª××¨ ××¢×× ×××ª××
    ××× ××¡×¤×¨ ×××¦× ××× ×Ö¾0 ××¢× ××××. ×××¦× ×× ×©×× ×× ×××× ×ª××¨, ××× ××ª××××¡××ª
    ××××ª×× ×× ×××××.

    ××¢×ª ×©××××© ××¦××¨× `×××ª××:××××` ×¢××× ××××× ×©×× ××¦× ×¢×××¨ ××ª××¨ ××§×× ××ª ×××ª×
    ×¢×¨× ××××.  ×× `×××ª××:××××` ×× ×¡××¤×§, ×× × ×¢×©× ×©××××© ×Ö¾1:1.
     
- ××¡×ª×××. 
- ×××ª×¢××××ª: 
- ×ª××¦×××ª: 
- ××× ×¢××××: 
××××¢××ª ×××××§××ª:
 
××× ×××× ×××××§××:
 
××× ×××× ×××××§××:
 
×¨×××× ×××§××/×××§ ××××¤× ×××××¤× ×××××¤× ××¨××©×× ×.
 
___________________________________________
×¡×× ×× ××ª××× ×©× Mailman ××¡××¨ ××ª ×××§×
×Ö¾MIME ××××× ××××××¢× ××××ª.
     A more verbose output including the file system paths that Mailman is
    using.     A specification of the mailing list to operate on.  This may be the posting
    address of the list, or its List-ID.  The argument can also be a Python
    regular expression, in which case it is matched against both the posting
    address and List-ID of all mailing lists.  To use a regular expression,
    LISTSPEC must start with a ^ (and the matching is done with re.match().
    LISTSPEC cannot be a regular expression unless --run is given.     ××××¡××£ ××ª ×× ××ª××××ª ××××¨×× ×©×Ö¾FILENAME ×¢× ××¦× ×××¡××¨× ×©×¦×××
    ×¢× â-d/--delivery.â  FILENAME ×××× ×××××ª â-â ××× ××¦××× ××ª ××§×× ××¨××× (stdin).
    ×©××¨××ª ×¨××§××ª ×× ×××× ×©× ×¤×ª×××ª ×Ö¾â#â ×× ×¢×××¨××ª ×¢××××.
         Add and delete members as necessary to syncronize a list's membership
    with an input file.  FILENAME is the file containing the new membership,
    one member per line.  Blank lines and lines that start with a '#' are
    ignored.  Addresses in FILENAME which are not current list members
    will be added to the list with delivery mode as specified with
    -d/--delivery.  List members whose addresses are not in FILENAME will
    be removed from the list.  FILENAME can be '-' to indicate standard input.
         ××××¡××£ ×/×× ×××××§ ××¢××× ×××¤×§××× ×©× ×¨×©×××.
         Additional metadata key/value pairs to add to the message metadata
    dictionary.  Use the format key=value.  Multiple -m options are
    allowed.     Change a user's email address from old_address to possibly case-preserved
    new_address.
         ×§×××¥ ××××¨××ª ××©××××©.  ×× ×× ×¦×××, ×××××¨××ª ×©×××¤××¢××ª ×××©×ª× × ××¡××××ª×
    MAILMAN_CONFIG_FILE ×ª×××× × ××¤×¢××××ª.  ×× ×× ×¦××× × ××£ ×××ª ×××,
    ×××¢× ×§×××¥ ××××¨××ª ×××¨×¨×ª ××××.     Create a mailing list.

    The 'fully qualified list name', i.e. the posting address of the mailing
    list is required.  It must be a valid email address and the domain must be
    registered with Mailman.  List names are forced to lower case.     ×ª××¨××: ${date}     Delete all the members of the list.  If specified, none of -f/--file,
    -m/--member or --fromall may be specified.
         Delete list members whose addresses are in FILENAME in addition to those
    specified with -m/--member if any.  FILENAME can be '-' to indicate
    standard input.  Blank lines and lines that start with a '#' are ignored.
         ××××§×ª ×××¨×× ××¨×©×××ª ×××××¨.     Delete the list member whose address is ADDRESS in addition to those
    specified with -f/--file if any.  This option may be repeated for
    multiple addresses.
         Delete the member(s) specified by -m/--member and/or -f/--file from all
    lists in the installation.  This may not be specified together with
    -a/--all or -l/--list.
         Discard all shunted messages instead of moving them back to their original
    queue.     ××¦××ª ×××¨× ×¨×©×××ª ××ª×¤××¦×.
    ××¤×©×¨ ××¡× × ××¤× ××××× ×ª× ××× ××¢×ª ×××¦××.
    ××©×× ××¦××× ×× ××¤×©×¨××××ª, ×××¦××× ×××¨× ××¨×©×××
    ×Ö¾stdout (×¤×× ××¡××£) ×××¦× ××¨××¨×ª ×××××.
         ××¦××ª ×× ××××¨××××ª ×©× ××©×ª××© ×× ××©×ª××©×× ×¢× ××ª×××ª ×©×ª××××ª
    ××ª×× ××ª. ××××× ×©×××§ ×××ª×××× ×××× ×××¨×ª ××ª×× ××ª ××©××××ª×ª SQL
    ×¢× ×ª××× ×××××, ××ª×× ××ª ×¦×¨×× ××××©××¨ ×¤×©×××. ×××¨×××ª ×¤×©×××
    ×¢××××ª ××× ×××.
         ×××¦×× ×¨×§ ×××¨× ×ª×§×¦××¨ ××¡×× ××¡×××.
    âanyâ ××©××¢× ×× ×¡×× ×ª×§×¦××¨ ×©×××, âplaintextâ ××©××¢× ×¨×§ ×ª×§×¦××¨××
    ×××§×¡× ×¤×©×× (rfc 1153), âmimeâ ××©××¢× ×ª×§×¦××¨×× ××¡×× MIME.     ×××¦×× ×¨×§ ×××¨×× ×¢× ROLE (×ª×¤×§××) ××¡×××.
    ××ª×¤×§×× ×××× âanyâ,â âmemberâ,â ânonmemberâ,â âownerâ,â âmoderatorâ,
    ×× âadministratorâ (×××××¨ ×××¢××× ×× ×××¤×§×××).
    ×× ×× ×¦×××, ××× ×× ××× ×©×××××¨ ××ª×¤×§×× âmemberâ.     ×××¦×× ×¨×§ ×××¨×× ×¢× ××¦× ××¡××¨× ××¡×××.
    âenabledâ ××××¨ ×× ××××¨×× ×©×××¡××¨× ××××× ×¤×¢×××, âanyâ ××××¨
    ×× ××××¨×× ×©×××¡××¨× ×©××× ××××× ××× ×¡××× ×©×××, âbyuserâ ××××¨
    ×××©×ª××©×× ××©×××ª× ××¢×¦×× ××ª ×××¡××¨×, âbybouncesâ ××××¨ ×××
    ×©×××¡××¨× ×××©××ª× ×¢× ××× ××¢×× ×××××¨××ª ×××××××××ª,
    âbyadminâ ××××¨ ×©×××¡××¨× ×××©××ª× ×¢× ××× ×× ×××ª ×× ××¤×§×× ××¨×©×××
    ×Ö¾âunknownâ ××××¨ ×©×××¡××¨× ×××©××ª×
    ××¡××××ª ×× ××××¢××ª (××××©×).     Display only memberships with the given role.  If not given, 'all' role,
    i.e. all roles, is the default.     ×××¦×× ×¨×§ ×××¨× ××¡××¨× ×¨×××××.     ××¦××ª ××¤×× ×Ö¾FILENAME ×××§×× ×Ö¾ stdout (×¤×× ×××¡××£).  FILENAME
    ×××× ×××××ª â-â ××× ××¦××× ××ª ××¤×× ×××¡××£ (stdout).     Don't actually do anything, but in conjunction with --verbose, show what
    would happen.     Don't actually make the changes.  Instead, print out what would be
    done to the list.     Don't attempt to pretty print the object.  This is useful if there is some
    problem with the object and you just want to get an unpickled
    representation.  Useful with 'mailman qfile -i <file>'.  In that case, the
    list of unpickled objects will be left in a variable called 'm'.     ×× ×××¦×× ××××¢××ª ××¦×.  ××××¢××ª ××©×××× ×¢×××× × ×©××××ª ××¢×¨××¥ ××©×××××ª
    ××¨××× (stderr).     ×× ×××¤×¢×× ××ª ×××¦× ×× ××××© ××©×× ××¡×ª××××× ×¢×§× ×©×××× ×× SIGUSR1 (×××ª ××©×ª××© 1).
    ××© ×××©×ª××© ××× ×× ××¤×× ×©×××××ª ××××.     ××ª×××ª ×××××´× ×©× ×××©×ª××© ×××¡×¨× ×××ª×¤×§×× ×©×¡××¤×§.
    ××¤×©×¨ ×××××¨ ×¢× ×× ××× ×××××§ ××× ××©×ª××©×× ×××.
         ×§×××¥ ×××× ××©××× ××ª ××¤××.  ×× ×× ×¦×××, ×× ×©×¦××× â-â, ×××¢×©× ×©××××©
    ××¤×× ××¨××× (stdout).     ×§×××¥ ×××× ×××©×× ××¤××.  ×× ×× ×¡××¤×§, ×× ×××× ××¤×× ××¨××× (stdout).     ×××ª: ${from_}     Generate the MTA alias files upon startup. Some MTA, like postfix, can't
    deliver email if alias files mentioned in its configuration are not
    present. In some situations, this could lead to a deadlock at the first
    start of mailman3 server. Setting this option to true will make this
    script create the files and thus allow the MTA to operate smoothly.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option, the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     ×× ×××©××× ××¨××©× ×××¦× × ×¢××× ×¨××©××ª ×§××××ª, ××× ×××¨× ××× ×××¦× ×¢× ××××¢×ª
    ×©××××. ×¢× ××¤×©×¨××ª ××, ×××©××× ×××××§ ××¨×× × ××¡×¤×ª. ×× ×ª×××× ×ª××× ××××¨×/
    ×××× ××©×ª××© ×©××ª×××¨ ××§×××¥ ×× ×¢××× × ××¦× ×¤×¢××, ×××©××× ×¢×××× ××¦×, ××
    ×©×××¨××© ××× ×× ×§××ª ××ª ×× ×¢××× ××× ××ª. ×× ×× × ××¦× ×ª×××× ×ª××× ×××©××× ××¡××¨
    ××ª ×× ×¢××× ×××××ª×¨×ª ××× ×¡× ×××©×× ××ª ×× ×¢××× ××¨××©××ª ×¤×¢× × ××¡×¤×ª.     ×× ××¨×¦×× × ××¨××¥ ×Ö¾root ××××¤× ××××, ××© ××××¡××£ ××ª â--run-as-root.     Import Mailman 2.1 list data.  Requires the fully-qualified name of the
    list to import and the path to the Mailman 2.1 pickle file.     Increment the digest volume number and reset the digest number to one.  If
    given with --send, the volume number is incremented before any current
    digests are sent.     Key to use for the lookup.  If no section is given, all the key-values pair
    from any section matching the given key will be displayed.     Leaves you at an interactive prompt after all other processing is complete.
    This is the default unless the --run option is given.     List only those mailing lists hosted on the given domain, which
    must be the email host name.  Multiple -d options may be given.
         ×¢××§× ×××¨ ×ª×ªÖ¾××ª×××× ××¨××©×.

    ××¤×¢×× ×××¢×§× ×××¨ ×××¦× ×× ××××××¨×× ××× ××××× ×©×× × ×©××¨×× ×¤×¢××××.
    ×× ××¦× ××ª×¤×¦× ××××× ×Ö¾exec ××ª××¨× ×ª×× ×©×ª×××× ××¢× ×××ª×× ×××××
    ××ª×××××× ×©×××. ×××©×¨ ××× ×××× ×©××¦× ×¦××¦× ×¡×××, ××× ×¢×©××
    ×××¤×¢×× ×××ª× ××××©.

    ×××¦× ×× ×××××× ×Ö¾SIGINT,â SIGTERM,â SIGUSR1 ××Ö¾SIGHUP.â  SIGINT,
    SIGTERM ×Ö¾SIGUSR1 ×××× ×××¨××× ×××¦× ××××¡××¨ ××¦××¨× × ×§×××.  ×ª×××× ××¢×
    ××¤×¢×× ××××© ××¦× ×× ×¢×§× SIGUSR1 ×× ×× ×¡×× ×××¨ ×©× ×ª× ×× ××¦×××
    (×××©× ×××× ×ª×§×× ×©×× ×××¤××).  SIGHUP ×××¨× ××ª×××× ××¢× ××××¦× ××
    ××¡×××¨ ××ª ×§×××¦× ××××× ×©××× ×××¤×ª×× ×××ª× ××××© ×¢× ×××¤×¢×ª ×××××¢×
    ××××.

    ×ª×××× ××¢× ×××× ×× ×Ö¾SIGINT,â SIGTERM,â SIGUSR1 ××Ö¾SIGHUP, ×©×××ª×
    ××× ×¤×©×× ××¢×××¨ ×××¦× ××.  × × ××©×× ×× ×©×ª×××× ××¢× ××¡×××¨ ×××¤×ª× ××××©
    ××ª ×§×××¦× ××××× ×©× ×¢×¦×× ×¢× ×§×××ª SIGHUP.  ×ª×××× ××¢× ××©×××¨ ××ª
    ×××× ××ª×××× ×©× ×¢×¦×× ××§×××¥ ×©×××¤××¢ ××§×××¥ ×××××¨××ª ×× ×××¨× ×××
    ××× ×× ×¡×× ××××©×ª××© ××××× ××ª×××× (PID) ××©××¨××ª.     ×××× ××××¢×: ${message_id}     Name of file containing the message to inject.  If not given, or
    '-' (without the quotes) standard input is used.     Normally, this script will refuse to run if the user id and group id are
    not set to the 'mailman' user and group (as defined when you configured
    Mailman).  If run as root, this script will change to this user and group
    before the check is made.

    This can be inconvenient for testing and debugging purposes, so the -u flag
    means that the step that sets and checks the uid/gid is skipped, and the
    program is run as the current user and group.  This flag is not recommended
    for normal production environments.

    Note though, that if you run with -u and are not in the mailman group, you
    may have permission problems, such as being unable to delete a list's
    archives through the web.  Tough luck!     ××××××¢ ×××¢×× ××¨×©××× ×××××´× ×©×¨×©×××ª ××××××¨ ×©×××
    × ××¦×¨×.     Operate on a mailing list.

    For detailed help, see --details
         Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the digests for all mailing lists.     ×ª×¤×¢×× ×¨×©×××ª ××××××¨ ××××ª.  ××¤×©×¨ ××¡×¤×§ ××¡×¤×¨ ××¤×©×¨××××ª â--list.  ×××¨×××× ×××
    ×××××× ×××××ª ×××× ×¨×©××× ×× ××©× ×××× ×©× ××¨×©×××.  ××× ×××¤×©×¨××ª ××××ª
    ××¤×¢××××ª ×ª×ª××¦×¢× × ×¢× ×× ×¨×©××××ª ××××××¨.     ××¨××¡×ª ×¢×¨××ª ×××¦× ×× ×××¨×¨×ª ×××× ×©××¨××©× ××¤×¢××, ×©××× ×××¨× ××× ×××××¨×ª
    ××§×××¥ ×××××¨××ª.  × ××ª× ××¡×¤×§ ××××× ××¤×©×¨××××ª â-r.  ××¢×¨××× ×¢×××¨ â-r ×××¢××¨××
    ××©××¨××ª ×× ×××× ×¨×/××¦×.     Override the list's setting for admin_notify_mchanges.     Override the list's setting for send_goodbye_message to
    deleted members.     Override the list's setting for send_welcome_message to added members.     Override the list's setting for send_welcome_message.     Register the mailing list's domain if not yet registered.  This is
    the default behavior, but these options are provided for backward
    compatibility.  With -D do not register the mailing list's domain.     ×××¤×¢×× ××ª ×××¦× ×©×¦××× ×××××§ ×¤×¢× ×××ª ××¨× ×××××× ××¨××©××ª ×©××. ×××¨×ª, ×××¦×
    ××¤×¢× ××××¤× ×××××¨× ×¢× ×©××ª×××× ××§×× ×××ª ×¡×××. ×× ×ª××× ×××¦× ×× ×©×× ××××××
    ××¤×¢×× ×¤×¢× ×××ª.     ×× ×××××¥ ×××¨××¥ ×¤×§××××ª ×©× mailman ××ª××¨ root ×Ö¾mailman ××¡×¨× ××¨××¥ ×Ö¾root
    ××××× ×¦××× × ××¤×©×¨××ª ××.     Section to use for the lookup.  If no key is given, all the key-value pairs
    of the given section will be displayed.     Send any collected digests for the List only if their digest_send_periodic
    is set to True.     Send any collected digests right now, even if the size threshold has not
    yet been met.     Send the added members an invitation rather than immediately adding them.
         Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.     Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.  Ignored for invited
    members.     Set the list's preferred language to CODE, which must be a registered two
    letter language code.     Specify a list owner email address.  If the address is not currently
    registered with Mailman, the address is registered and linked to a user.
    Mailman will send a confirmation message to the address, but it will also
    send a list creation notice to the address.  More than one owner can be
    specified.     ×¦××× ×©× ×¡×× ×× ×¨×©×××.     Specify the encoding of strings in PICKLE_FILE if not utf-8 or a subset
    thereof. This will normally be the Mailman 2.1 charset of the list's
    preferred_language.     ××¤×¢××ª ××¦×.

    ×××¦× ×©×¦××× ××©××¨×ª ××¤×§××× ×××¤×¢× ×××× ×××× ××¤×¢×× ××¨× ×××××× ××¨××©×× ×
    ×©×× ×¢× ×××ª (×××¦× ×× ×©×ª××××× ×××) ×× ××××¤× ×¨×¦××£. ×××¤×©×¨××ª ××©× ×××
    ××× ×××¨× ×× ×××¦× ××¨××©× ××¤×¢×× ××ª ×× ×ª×ªÖ¾××ª×××××× ×©××.

    â-r ××¨××© ××××× ×¡××¤×§× â-l ×× â-h, ××××©×ª× ×× ×©×× ×××××× ×××××ª ××× ×××©×××ª
    ×©××¦××× ×× ×¢× ××× ××¤×§××× â-l.

    ×××¨× ×××, ××ª ××¡×§×¨××¤× ××× ××© ×××¤×¢×× ××¨× `mailman start`.  ××¤×¢××ª×
    ×× ×¤×¨× ×× ×¢× â-o ×©××××©××ª ×××¨× ××× ×× ××¤×× ×©×××××ª.  ××¤×¢×× ××××¤× ×××
    ×××××× ××× ×©×××©×ª× × ××¡××××ª× â$MAILMAN_UNDER_MASTER_CONTROL
    ×××××¨, ×× ×©××××× ××©×× ×××× ×§××× ××××§ ×××ª× ××××ª ××××¤×× ××©×××××ª.
         Start an interactive Python session, with a variable called 'm'
    containing the list of unpickled objects.     × ××©×: ${subject}     The gatenews command is run periodically by the nntp runner.
    If you are running it via cron, you should remove it from the crontab.
    If you want to run it manually, set _MAILMAN_GATENEWS_NNTP in the
    environment.     ××¨×©××× ××ª×¤×¢××.  × ×××¥ ××××× ×¦××× â--fromall.
         The name of the queue to inject the message to.  QUEUE must be one of the
    directories inside the queue directory.  If omitted, the incoming queue is
    used.     ××ª×¤×§×× ××××¡×¤×/××¡×¨×. ×××× ×××××ª âownerâ ×× âmoderatorâ (××¢×××ª ×× ×¤××§××).
    ×× ×× ×¡××¤×§, ×× ×××××¨ ×ª×¤×§×× ×××¢×××ª (âownerâ).
         ×××¤×©×¨××ª ×× ××× ××©×¤×¢×. ××× ×§××××ª ××××¨××ª ×ª×××× ×××¨×¡×××ª ××§×××××ª ××××.     ××©×ª××© ××××¡×¤× ×¢× ××ª×¤×§×× ×©×¦×××. ×××ª ××××× ×××××ª ××ª×××ª ××××´×, ××
    × ××¡×¤× ××¡×××¨×××, ×× ×©× ×ª×¦××× ×××ª×××ª ××××´× × ××ª× ×× ××¤×¢× ×× ××ª××¨
    email.utils.parseaddr. ×××©×: 'Yaron Doogma <yaron@doogma.com>'. May be repeated
    ××× ××××¡××£ ××× ××©×ª××©.
         [MODE] Add all member addresses in FILENAME.  This option is removed.
    Use 'mailman addmembers' instead.     [MODE] Delete all member addresses found in FILENAME.
    This option is removed. Use 'mailman delmembers' instead.     [MODE] Synchronize all member addresses of the specified mailing list
    with the member addresses found in FILENAME.
    This option is removed. Use 'mailman syncmembers' instead.  (××¦× ×ª×§×¦××¨) × ××¦×× ${count} ×¨×©××××ª ×××××¨ ××ª×××××ª: ×××¨× ××¤×¨×¡×××× ×©× ${display_name} ${email} ×××¨ ${role.name} ×Ö¾${mlist.fqdn_listname} ${email} ×× ${role.name} ×Ö¾${mlist.fqdn_listname} ×××× ×× ×©× ${member} ××¨×©×××ª ××××××¨ ${mlist.display_name} ×××× ×¢×§× ××××¨××ª × ××§×× ×××××¨× ×©× ${member} ××¨×©××× ${mlist.display_name} ××× ×××× ×× ×©× ${member} ×¢× ${mlist.display_name} ×××× ×ª×§×¦××¨ ×©× ${mlist.display_name}, ××¨× ${volume}, ×××××× ${digest_number} ××××¢×ª ×ª×©××× ××× ×¨×©×××ª ××××××¨ ${mlist.display_name} ××××¢×ª ××× ×× ××× ${mlist.display_name} ××××¢×ª ××××× ××× ×× ×××¨×©××× ${mlist.display_name} ××¤×¨×¡×× ×©× ${mlist.fqdn_listname} ×××ª ${msg.sender} ×××¨×© ×××©××¨ ${mlist.list_id} ×§×¤×¦× ×× ×¤× ${mlist.volume}, ××¡×¤×¨ ${mlist.next_digest_number} ××¨×©××× ${mlist.list_id} ××× ×××¨×× ${mlist.list_id} is at volume ${mlist.volume}, number ${mlist.next_digest_number} ${mlist.list_id} ×©××× ××ª ×× ×¤× ${mlist.volume}, ××¡×¤×¨ ${mlist.next_digest_number} ${name} ××¤×¢×× ××ª ${classname} ××§×©×ª ××¨×©×× ×©× ${person} ×× ${listname} ×××ª×× × ${person} ××××¥ ××¨×©××× ${mlist.fqdn_listname} ${realname} ××¨× ${mlist.display_name} ${self.email} ×××¨ ×××§ ××¨×©×××ª ××××××¨ ${self.fqdn_listname} {self.email} ×××¨ ××ª×¤×§×× ×¤××§×× ××¨×©×××ª ××××××¨ {self.fqdn_listname} ${self.email} ×××¨ ×× ××¨×©×××ª ××××××¨ ${self.fqdn_listname} ×××¢×××ª ×¢× ×¨×©×××ª ××××××¨ ${self.fqdn_listname} ×××¨ ×××× {self.email} ${self.name}:â ${email} ××××¥ ××¨×©××× ${mlist.fqdn_listname} ${self.name}: ×× × ××¦×× ××ª×××ª ×ª×§×¤× ××¨××©×× ${self.name}: ×× × ××¦×× ××ª××××ª ×ª×§×¤××ª ×××××× ×××× ×× ${self.name}: ××× ×¤×§××× ××××ª: ${command_name} ${self.name}: ×××ª×¨ ××× ××©×ª× ××: ${printable_arguments} (××× × ××©×) - ×¤×¨×× ×××××¢× ×××§××¨××ª: -------------- ××××§ ××× --------------
 --send and --periodic flags cannot be used together <----- ××ª×××ª ××¤×¨×× ${count} -----> ×××§ ×××××¢× ×©××× × ×ª××× ××ª×§×¦××¨×× ×××§×¡× ×¤×©×× ×××¡×¨ â¦
×©×: ${filename}
×¡××: ${ctype}
××××: ${size} bytes
×ª××××¨: ${desc}
 ××¤×¢×× ×§××××ª ×©× GNU Mailman ×× ××¡×ª×××× ××¨××× ({}).  ×¢×××£ ×× ×¡××ª ×××©×ª××© ×Ö¾â--force ××× ×©×ª××× ×ª×××. ××§×× ××××¢×. ×××¡×¤×ª ×§×××××ª ×××××××ª ××¨×©××× ××¢×¨× ×××ª×¨×ª ×× ××©× (Subject). ×××¡×¤×ª ×××ª×¨××ª List-*â ××¤× ×ª×§× RFC 2369. ×××¡×¤×ª ×××××¢× ×××¨×××× ××. ×××¡×¤×ª ×××××¢× ××ª×§×¦××¨, ×¢×©××× ×× ××©××× ×××ª×. ×××ª×××ª ×××××¤× ×Ö¾{} ×Ö¾{}. ×××ª×××ª {} ×××¨ ×§×××, ×× × ××ª× ××©× ××ª. ×××ª×××ª {} ××× × ××ª×××ª ××××´× ×ª×§× ××ª. ×××ª×××ª {} ×× × ××¦××. ×××ª××××ª ××× × ×©×× ××ª.  ××× ×©×× ×××× ××××¦××¢. ××××¨ ×¡×× ×× ××ª×××, ×××××¢× × ××ª×¨× ×¨××§× ×××¨ ×××¦×¢ ×¨××©×× (××××): ${email} ××× ×× ×Ö¾âendâ (×¡×××). ××× ×× ×Ö¾âjoinâ (××¦××¨×¤××ª). ××× ×× ×Ö¾âleaveâ (×¢××××). An alternative directory to output the various MTA files to. And you can print the list's request address by running:

% mailman withlist -r listaddr.requestaddr -l ant@example.com
Importing listaddr ...
Running listaddr.requestaddr() ...
ant-request@example.com ×¡×× ×× ×¨×©×××ª ×××××¨ ××××¨×××ª ××××. ××××ª ××¤×××ª× DMARC. As another example, say you wanted to change the display name for a particular
mailing list.  You could put the following function in a file called
`change.py`:

def change(mlist, display_name):
    mlist.display_name = display_name

and run this from the command line:

% mailman withlist -r change -l ant@example.com 'My List'

Note that you do not have to explicitly commit any database transactions, as
Mailman will do this for you (assuming no errors occured). ×ª×××× ××××××××ª ×××××¢× ×©×× ×× ×¨×©×××ª ××××××¨ â${display_name}â ××¤×¨× ××¦× ×©×××: ${value} ×××©×× × ××¢× × ×××¢×××ª ×××¤××§××. ×××©×× ×× ××¢× ×× ××¨××××× ×©× ×××××¢×. ×× × ××ª× ××××× ××ª ××××× ×××¦×: ${class_path} ×× × ××ª× ××¤×¢× × ×××ª×××ª ××××´× ×ª×§× ××ª (××××): ${line} ×× × ××ª× ×××©×× ××ª ×××××¢× ${filebase} ××ª××¨ ××¡×§××¨×, ×××©××××:
${error} ××ª×¤××¡ ××××¢××ª ××××××ª ×××××× ×××¨×× ×©×¦×××. ××ª×¤××¡ ××××¢××ª ×¢× × ××©× ×ª×§×¦××¨ ×× ×¢× ×¦×××× ××§×××. ××ª×¤××¡ ××××¢××ª ×¢× ××¢× ××¨×××. ××ª×¤××¡ ××××¢××ª ×©××× ××× ×××ª×¨××ª × ××©× (Subject) ×× ×©×× ×¨××§××ª. ×ª×¤××¡×ª ××××¢××ª ×¢× ×××ª×¨××ª ××©××××ª. ××ª×¤××¡ ××××¢××ª ×¢× ×××ª×¨ ××× × ××¢× ×× ××¤××¨×©××. ××ª×¤××¡ ×¤×§××××ª ××××´× ×× ××¢× ×©×××. ×××¡××¨ ×××ª×¨××ª ××¡×××××ª ××× ×××××¢××ª. ×××©××¨ ××× ×× ×× ××§×©×ª ××××¢× ×××××§×ª. × ×©××× ×× ${person} ××××¢×ª ×××××ª ×××××´× × ×©××× ××××¢×ª ××××´× ××××©××¨ ××¢×××× ×©× ${person} ×××¨×©××× ${mlist.fqdn_listname} ××¡×××× ××××××ª ×× ××ª××× ××© ×××©××¨ ××ª×¨××ª ×¡×× ×× ×ª××× ××××¢× × ××¦×¨× ×¨×©×××ª ×××××¨: ${mlist.fqdn_listname} ×¤××§×× DMARC ×¢××¦×× ××××¢× ×¢× ×××ª×¨×ª ×¢×××× × ××ª××ª×× ×. ×××ª×¨×ª ×ª×§×¦××¨ ×ª××ª×× × ×××ª×¨×ª ×ª×§×¦××¨ ×¢×××× × ××©×××ª ×××××¢× ××¢×¦××¨×ª ××¢××××. ×¡×× ×× ×¨×©×××ª ×××××¨ ××××× ×× ×¢× ××¨×××× ×× ×¤×¨××××. Display Mailman's version. ××¦××ª ×¤××¨×× × ××¤×× ×©×××××ª × ××¡×£ ××§×××¥ ×××××. ×¤××××ª ×××©×ª× ×× ×©×× ××××¨×. ××××´×: {} ×¢× ××¨×©××× ××××ª ×××¤×¢× ×¤××§×× ×××¨×× ×¡××£ ×©×  ×¡×× ×× ×ª××× ×Ö¾MIME ×©× ××××¢××ª. ×××ª××¨ ×××× ×××ª ×Ö¾DMARC ×©× ×©× ××ª××× ××©×× From:â (×××ª). ××¡××××ª ×× ××××¢××ª, ×× × ××ª× ×××©×× × ×¢××× ×¨××©××ª.

×§×××¥ × ×¢×××: ${config.LOCK_FILE}
×××¨× × ×¢×××: ${hostname}

××ª×× ××ª ×××¦××ª. ××¢××¨× ×©× ××××¢× ××¤××§××ª GNU Mailman ×××¨ ×¤×¢×× GNU Mailman × ××¦× ×××¦× ×××ª× ×¦×¤×× (${hostname} != ${fqdn_name}) GNU Mailman ××× × ×¤×¢×× GNU Mailman ×¤×¢×× (×××× ×ª×××× ×¨××©×: ${pid}) GNU Mailman × ×¢×¦×¨ (×××× ×ª×××× ××××©×: ${pid}) Generating MTA alias maps ×§×××ª ×¨×©××× ×©× ×××¨× ××¨×©×××. ×§×××ª ×¢××¨× ×¢× ×¤×§××××ª ×××××´× ××××× ××ª. ×§×××ª ××××¢ ××§×××¥ ×ª××¨. ×§×××ª × ××¢× × ××§××× ××¨××××× ××§×××¥ ×××××. ××××ª×¨×ª â{}â ×ª××××ª ××©××¨× ×Ö¾bounce_matching_header (××××¨×ª ×××ª×¨×ª ×ª××××ª) ××××ª×¨×ª â{}â ×ª××××ª ×××× ×××ª×¨×ª Here's an example of how to use the --run option.  Say you have a file in the
Mailman installation directory called 'listaddr.py', with the following two
functions:

def listaddr(mlist):
    print(mlist.posting_address)

def requestaddr(mlist):
    print(mlist.request_address)

Run methods take at least one argument, the mailing list object to operate
on.  Any additional arguments given on the command line are passed as
positional arguments to the callable.

If -l is not given then you can run a function that takes no arguments.
 ×¢×××× ××××¢× ×××¤×¡×§×ª ××¢××××. ×¢×¦× ××ª×××× ×××××¢× ×× ×ª×× ×©×××¨× ×¢× ×××ª×¨×ª ×Ö¾Subject:â (× ××©×) ××¤× ×©×××,
×ª××¨×× ×Ö¾Mailman ×××ª×¢×× ××××××¢× ××××××§×ª. ×××××¥ ××¢×©××ª ×××ª ×× ×××ª
××××¢×ª ×¡×¤××. ×©××××ª ×ª×××× ×××××¢× ×× ×ª×× ×××××ª ××××ª×¨×ª Approved:â (××××©×¨)
×¢× ×¡×¡××ª ××¨×©××× ××ª×××, ×××××¢× ×ª×××©×¨ ××¤×¨×¡×× ××¨×©×××. ××××ª×¨×ª Approved:â
××××× ×× ××××¤××¢ ××©××¨× ××¨××©×× × ×©× ×××£ ××ª××××. ×××ª× ××××× ×, ××ª×¢××: {0!r} ×××ª×¢×× ×××××§×× ×©××× × ××§×¡×/MIME ×¤×©×× ×©× ××¨×©××× ××× × ×ª×§×£: ${fqdn_listname} ××ª×××ª ××¢××× ×××ª× ×ª×§×¤×: ${invalid} ××××¢ ×¢× ××¢××ª×§ ××× ×©× Mailman. Inject a message from a file into a mailing list's queue. ×××©××¨ ×©×××: ×¡×¡×× ××ª×××ª ××××´× ×©××××: ${email} ×§×× ×©×¤× ×©×××: ${language_code} ××ª×××ª ××××´× ×©×××× ×× ×××ª× ×××××ª×ª: ${email} Invalid value for [shell]use_python: {} ××ª×××× ××¨××©× ×¤×¢×× ××××? ××¦××¨×¤××ª ××¨×©×××ª ×××××¨ ××. ××ª×¨××ª ×ª×××× ××××××××ª ×××¨×× × ××××× ××¢××× ××ª ×¨×©×××ª ××××××¨ ×××. ××¢××× ××ª ×¨×©×××ª ××××××¨ ×××.

×××× ×××××ª ×©×××× ×¢××× ××××ª ××ª ××§×©×ª×. ×¤×××ª ×¤××¨×× List all mailing lists. ××¨×©××× ×××¨ ×§××××ª: ${fqdn_listname} ×××¦×× ×¨×§ ××ª ×¨×©××××ª ××××××¨ ×××××××ª ××¦××××¨ ×××¦×× ××ª ×©×××ª ×××¦× ×× ××××× ×× ×××¦××ª. ×¨×©×××: {} ×××¤××© ×××××ª ×¤×¨×¡××. ×××¤×© ××ª××× ×××× ×§××× ×××©××. ××ª×××ª ×× ×××××¢××ª ×©×¤××¨×¡×× ××¨×©×××ª ×××××¨ ×× ××××¢× ××ª
        ××§×××¦×ª ×××©××ª ××¤××§××ª.
         ××ª×××ª ××××¢××ª ×©× ×©××× ×××ª××××ª ××¡××××ª. ××ª×××ª ××××¢××ª ×©× ×©××××ª ×¢× ××× ×××¨×× ××¤××§××. ××ª×××ª ××××¢××ª ×©× ×©××××ª ×¢× ××× ×××× ×©××× × ×××¨××. ××ª×××ª ××××¢××ª ××× ×©××××× ×ª×§×¤××. ××××¨ ××× × ×× ×× (××××): ${email} ×××¨× ×¨×©×××ª ××××××¨ {}:
{} ××××¨××ª ××¡××× (××××): ${email} ××××¢× ${message} ××××¢××ª ×©××××××ª ×¤×§××××ª × ×××××××ª ×××××¢× ××© × ××©× ×©× ×ª×§×¦××¨ ×××××¢× ××××ª ×××¨ ×¤××¨×¡×× ××¨×©××× ××××ª ×××××¢× ××© ××¢× ××¨××× ×××××¢× ××© ×××¢×× ×Ö¾{} × ××¢× ×× ×××××¢× ××× × ××©× ××××¢× ×©××¦×××ª ××§××× ×©× ×ª×§×¦××¨ ×××¢× ×××××¢× {} ××¡×× ××× ××¨×©××× ××××ª ××¡×¨×× × ×ª×× ×× ×××§×©× {}

 ×©×¨×©×¨×ª ×¤××§×× ×©×× ×× ×××ª×¨××ª ×××××¢×. ×××¢×××¨ ××ª ×××××¢× ××ª××¨ ××××©××ª ××××¦×××ª. ×× ×××× ×©×: ${fname}
 ××§×©×ª ××× ×× ×××©× ×× ${self.mlist.display_name} ×××ª ${self.address.email} ××§×©×ª ××××× ××× ×× ×××©× ×××¨×©××× ${mlist.display_name} ×××ª ${email} ××§×©×ª ××××× ××× ×× ×××©× ×××¨×©××× ${self.mlist.display_name} ×××ª ${self.address.email} ××× ×¦××¦× ×¢× ×××× ××ª××××: ${pid} ×× × ××¦× ××¡×××× ×××××ª ×× × ××¦×× ×¨×©××××ª ×××××¨ ××ª×××××ª ××× ××©×ª××© ×¨×©×× ×¢× ××ª×××ª ×××××´×: ${email} ×× ×¡××¤×§ ×©× ××¦×. ×× × ××¦×× ×©××××× ×××××¢×. ××× ×¤×§××× ××××ª: ${command_name} ×× × ××¦×× ×¨×©××× ××××ª: ${spec} ××× ××¤×¨× ××× ××¨×©×××ª ×××××¨: ${listspec} ××× ××××ª ×¨×©×××: ${_list} ××× ××××ª ×¨×©×××: ${listspec} ××× ××× ×ª××¨: ${queue} ×× ×§×××¥ ××××¨××ª ×©× Mailman 2.1â: ${pickle_file} ××× ×× ××××¡××£ ×× ×××××§. ××× ×¤×¢××××ª ××××¦××¢ ××××××¢ ×××¢××/××¤×§×× ××¨×©××× ×¢× ××§×©××ª ×××ª×× ××ª. ××¡×¤×¨ ××¤×¨×××× ×©× ××¦×× (×××× ×××©×ª× × âmâ): ${count} Operate on digests. ×¡×× ×× ×¨×©×××ª ×××××¨ ×¨×××× ××××× ××. ××××¢× ××§××¨××ª ×××× ××ª×××× ××× × ×§×¨×× ×ª××ª: ${config.PID_FILE} ×××¦××¢ ××××§××ª ×××××ª ARC (×©×¨×©×¨×ª ×××¢×× ××××× ×) ×××¦×¨×£ ××ª ××××ª×¨××ª ×××ª×§××××ª ×××¦×¢ ××××§××ª ×××××ª ×××¦×¨×£ ×××ª×¨×ª Authentication-Results (×ª××¦×××ª ×××××ª). ×¢×¨×××ª ×¤×¢××××××ª ×× ×××ª×××ª ××××¨ ×¤×¨×¡×× ×××¦××. Poll the NNTP server for messages to be gatewayed to mailing lists. ×¤×¨×¡×× ×× ××¢× ×§×××¦×ª ×××©××ª ××¤××§××ª ×¤×¨×¡×× ×××××¢× ×©×× ×¢× ×× ××©× â${subject}â ××¦××ª ×× ××××ª ××¤××¨×××ª ×××¦××ª. ××××¤××¡ ×¤×××ª ×¤××. Print some additional status. Print the Mailman configuration. ×¢×××× ×××××ª DMARC ×× ××ª×¢××××ª ××××¤×××ª×× ××¤××§ ×¨×©××× ×©× ×©×××ª ×××ª××××ª ×××××´× ×©× ××××¨××.

× ××ª× ×××©×ª××© ×××©×ª× × ××¨×©××ª ×× ××¡×¤×× delivery=â ×Ö¾mode=â ××× ×××××× ××ª
×××× ×××©×ª××©×× ×©×¢×× ×× ×¢× ××¦× ×/×× ×ª×¦××¨×ª ××©×××. ×× ×¦××× × delivery=â
×× mode=â ×××ª×¨ ××¤×¢× ×××ª, ×¨×§ ××¦××¨××£ ××××¨×× ××××©×.
 Programmatically, you can write a function to operate on a mailing list, and
this script will take care of the housekeeping (see below for examples).  In
that case, the general usage syntax is:

% mailman withlist [options] -l listspec [args ...]

where `listspec` is either the posting address of the mailing list
(e.g. ant@example.com), or the List-ID (e.g. ant.example.com). ×¡×××: {}

 Regenerate the aliases appropriate for your MTA. ××××× ×¨××××¨× ×××¨×© â--run ×××××ª/××§×¤×¦×ª ××××¢× ×××¤×¡×§×ª ××¢××××. ××¡×¨×ª ×××ª×¨××ª DomainKeys. ×¨×©××× ×××¡×¨×: ${listspec} ×××¦× ×× ×©× Mailman × ×¤×ª××× ××××© ×××§×©× ××¨×©×××ª ××××××¨ â${display_name}â × ×××ª× ×××¦× ×× ×©× Mailman ×××¤×¢××× ××××© ×©××××ª ×××¨× ×¢× ×¤×¨×¡××. ×©××××ª ×ª×××××ª ×××××××××ª. ×©××××ª ×××××¢× ××ª××¨ ××××¦×. ×××¢×: {}
 Show a list of all available queue names and exit. ×××¦×× ×× ××ª ×ª××××¨× ××¨×©××××ª ×××¦×× ×× ××ª ×©×××ª ××¨×©××××ª ×××¦×× ××ª ××¦× ××¤×¢××××ª ×× ×××× ×©× ××¢×¨××ª Mailman. ×××¦×× ××ª ××××¢×ª ××¢××¨× ××××ª ×××¦××ª. ×××¦× ××¨××©× ×©× Mailman × ××× Signal the Mailman processes to re-open their log files. ×§×××¥ ×××× ×ª×××× ××ª×× ×××¡×¨. Start the Mailman master and runner processes. ×××¦× ××¨××©× ×©× Mailman ×××¤×¢× ××¢×¦××¨ ××××¤×¢×× ××××© ××ª ×ª×ªÖ¾×ª××××× ×××¦× ×× ×©× Mailman. ×¢×¦××¨×ª ×¢×××× ×¤×§××××ª. ××¢×¦××¨ ××ª ××ª×××× ××¨××©× ×××ª ×ª×ªÖ¾××ª×××××× ×©× ×××¦× ×× ×©× Mailman. × ××©×: {}
 ×××× ×× ×××¨ ×××ª×× (××××): ${email} ××§×©×ª ×¨××©×× ××××××§ ×¢××ª×§×× ××©×××¤××× ×©× ×××ª× ×××××¢×. ××ª×¢××××ª ×××××¢××ª ××¦× ×ª××× ××××¢××ª ×¢× ××ª×××ª × ××©×. ××× ××ª×××××ª ×©× Mailman ×××××¢× ×××¦××¨×¤×ª ×ª××××ª ××××× ×¡×× ×× ××ª××× ×©× ×¨×©×××ª ××××××¨
${mlist.display_name} ×××¢××¨×ª× ××××¨× ×¨×©×××ª ××××××¨ × ×× ×¢×.
×××× ××¢××ª×§ ×××××× ×©× ××ª×¨ ××××××¢× ×©×××©×××.

 ×©×¨×©×¨×ª ××¤×¨×¡×× ×××¢××× (â-owner) ××××× ××ª. ×©×¨×©×¨×ª ××ª×××ª ××××ª×¨××ª ××××× ××ª ×©×¨×©×¨×ª ××¤××§×× ××××× ××ª. ×ª×¦××¨×ª ×××ª××¡×¨××ª ××××× ××ª ×××¢×××. ×ª×¦××¨×ª ××¤×¨×¡×× ××××× ××ª. ×ª××× ×××××¢× ××××ª ×××. ×× ×¨×× ×©××× ×¤××¨×¡× ×××§××× ×××¡×¤×¨ ×¨×©××××ª
××××¨ ×××¤× ××¢××¨ ××¨×©××× ×××¨×ª.
 ×¨×©×××ª ××××××¨ × ××¦××ª ×××¦× ××××§×ª ×××¨×× ××××××¢× ××××ª ××
        ×××©×¨× ××¨××© ×¢× ××× ×× ×××ª ××¨×©×××.
         ×× × ××ª× ×××©×× ××ª ×× ×¢××× ××¨××©××ª ××××× ×©× ×¨×× ×©××© ×××¨ ×©××¨××ª
×××¨× ×××¨ ××××. ×× × ××ª× ×××©×× × ×¢××× ×¨××©××ª ××××× ×©× ×¨×× ×× ×ª×××× ×××¨ ×× ×©×××¨× ×××¨ ××©×× ×××ª×.
××× ×× × ××¤×©×¨××ª ×××××§ × ×¢××××ª ××××©× ××ª ××¢××¨ ×××××××ª ××××¨×, ××× ×××× ×¢××× ×× ×§××ª
××ª ×× ×¢××× ××××ª ××× ××ª.

×§×××¥ × ×¢×××: ${config.LOCK_FILE}
×××¨× × ×¢×××: ${hostname}

××ª×× ××ª ×××¦××ª. ×× × ××ª× ×××©×× ××ª ×× ×¢××× ××¨×©×××ª. × ×¨×× ×©××© × ×¢××× ×¨××©××ª ××××©× ×ª. ×××× ×× ×¡××ª
×××¨××¥ ××ª ${program} ××××© ×¢× ×××× â--force. ×××××¢× ××××¢× ××××¨ ××¤××§×× ×××××¢× ××© ×××ª×¨×ª Approve (×××©××¨) ×× Approved (××××©×¨) ××ª××××. ×××××¢× ××× ×©××××× ×ª×§×¤×× ×××××¢× ××××× ××××××× ×××¨×××ª ×©× {} ×§×´× ×××××¢× ××× ××××¨ ××××¥ ××¨×©××× ×¡×× ××ª××× ×©× ×××××¢× × ××¡×¨ ×××¤××¨×© ×¡×× ××ª××× ×©× ×××××¢× ×× ×××©×¨ ×××¤××¨×© ×¡××××ª ××§×××¥ ×©×××××¢× × ××¡×¨× ×××¤××¨×© ×¡××××ª ××§×××¥ ×©×××××¢× ×× ×××©×¨× ×××¤××¨×© ×ª××¦×××ª ×¤×§××××ª ×××××´× ×©×× ×××¤××¢××ª ××××.
 ×ª××¦×××ª ×¤×§××××ª ×××××´× ×©×× ××©××× ××× × ××¨×©×××ª ××××¨×× {} The variable 'm' is the ${listspec} mailing list ×ª×¦××¨×ª ××¤×¨×¡×× ×××ª×××. ××¨×©××× {} ××© {} ××§×©××ª ×¤××§×× ×××ª×× ××ª. There are two ways to use this script: interactively or programmatically.
Using it interactively allows you to play with, examine and modify a mailing
list from Python's interactive interpreter.  When running interactively, the
variable 'm' will be available in the global namespace.  It will reference the
mailing list object. This script provides you with a general framework for interacting with a
mailing list. × ××©×× ×××× (${count} ××××¢××ª) × ××©×× ××××:
 ×©× ×ª××× ×× ×××××¨: ${domain} ×©× ×××¦× ×× ×××××¨: ${name} ×©× ×¡×× ×× ×¨×©××× ×× ××××¢: ${style_name} ××©×ª× ×× ×× ××××¨×× ×× ×©×××××:
{} Unshunt messages. ××§×©×ª ××××× ××× ×× ××©×ª××©: {}
 ××¨×× ×××× ×× ×¨×©×××ª ××××××¨ â${mlist.display_name}â${digmode} ××¡××¨ ×× ××¤×¨×¡× ××¨×©××× ××××ª ××©× ×ª××× ×Ö¾From:â (×××ª) ×©××¤×¨×¡× ×××× ×××ª DMARC ×©× ××××× ×× ××××× ××××××¢× ×©×× × ×××ª× ××××××××ª. ×× ×××¢×ª× ×××××¢××ª ×©×× × ××××ª ××©×××, × × ×××¦××¨ ×§×©×¨ ×¢× ××¢×× ×¨×©×××ª ××××××¨ ×××ª×××ª ${listowner}. ××× ×× ××¨×©×× ××¦×¤××ª ××¨×©×××ª ××××¨××. You can print the list's posting address by running the following from the
command line:

% mailman withlist -r listaddr -l ant@example.com
Importing listaddr ...
Running listaddr.listaddr() ...
ant@example.com ××××× ×ª ×××¦××¨×£ ××¨×©×××ª ××××××¨ ${event.mlist.fqdn_listname}. ×××× ×× ×©×× ××¨×©×××ª ××××××¨ ${mlist.display_name} ×××× ×ª××¤××¢ ××¤× ×× ××§×©× ×××©×¨ ××ª ××§×©×ª ×××× ×× ×©×× ××ª×× ×¤×§ ×¢×××¨× ×¡×¡×× ××× ××ª.

×¢× ××× ×©××××© ×××¤×©×¨××ª âdigestâ (×ª×§×¦××¨), × ××ª× ××¦××× ××× ××¢× ××× ×××ª× ××§××
×ª×§×¦××¨ ×× ××. ×× ×× ×¦××× ×ª ×××¨, ×××¢×©× ×©××××© ××©×××ª ×××©××× ×××¨×¨×ª ×××××
×©× ×¨×©×××ª ××××××¨. × ××ª× ×××©×ª××© ×××¤×©×¨××ª âaddressâ (××ª×××ª) ××× ×××§×©
××× ×× ×©× ××ª×××ª ×©×× × ××× ×©××× × × ×©××× ×××××¢×.
 × ××¨×© ×××××ª ××¦×× ××× ×××¦××¨×£ ××¨×©×××ª ×××××¨ ${event.mlist.fqdn_listname}. × ××¨×© ×××××ª ××¦×× ××× ××¢××× ××ª ×¨×©×××ª ××××××¨ {event.mlist.fqdn_listname}. ××××¢×ª× ×× ${mlist.fqdn_listname} ×××ª×× × ××××©××¨ ××¤××§×× ×¨×©×××ª ××××××¨ ××××©× ×©××: ${fqdn_listname} ×××× ×× ×©×× ××¨×©×××ª ××××××¨ ${mlist.display_name} ×××©××ª ×××××¢× ×××××¤× ×©×× ××¨×©×××ª ××××××¨ ${mlist.display_name} ××
×××©×¨× ×××¡××¨×.  ×××××¢× ×××§××¨××ª ××¤× ×©××ª×§××× ×¢× ××× Mailman
××¦××¨×¤×ª.
 [${mlist.display_name}]  [----- end pickle -----] [----- start pickle -----] [×××¡×¤×] %s [××××§×] %s [×¤×¨×× ×××××¨× ××× × ×××× ××] [××× ×¤×¨××× ×××× ××] [×× ×¡××¤×§× ×¡×××] [×× ×¦××× × ×¡×××] ××©×ª× × ×©×××: ${argument} ×¨×©×××ª ××××××¨ â${listname}â × ××¦×¨× ××¨××¢ ××©××××. ×××× ×¤×¨××× ××¡××¡××× ×¢×
×¨×©×××ª ××××××¨ ×©××.

××© ×× ×©×§ ××××¡×¡ ××××´× ×××©×ª××©×× (×× ××× ×××) ××¨×©××× ×©×× ××××¦×¢××ª× × ××ª×
××§×× ××××¢ ××¨× ×©××××ª ××××¢× ×¢× ×××× ××××× ×××× âhelpâ (×¢××¨×) ×× ××©× ×× ××××£
×××××¢× ××:

    ${request_email}

× × ×××¤× ××ª ××ª ×× ××©××××ª ×× ${site_email}. ×¢××¨× ×¢×××¨ ×¨×©×××ª ××××××¨ ${listname}

×× ×¤×§×××ª ×××××´× âhelpâ (×¢××¨×) ×¢×××¨ ××¨×¡× ${version} ×©× ×× ×× ××¨×©××××ª GNU Mailman
×ª××ª ${domain}.  ×××× ××ª×××¨××ª ×¤×§××××ª ×©× ××ª× ××©××× ××× ××§×× ××××¢ ×××©××× ×××× ××
×©×× ××¨×©××××ª ×©× Mailman ×××ª×¨ ×××.
×¤×§××× ×××× ×××××ª ××©××¨×ª ×× ××©× ×× ××××£ ×××××¢×.

××© ××©××× ××ª ××¤×§××××ª ×××ª×××ª â${listname}-request@${domain}.

×× ×××¢ ××ª××××¨×× - ××××× ××ª×× â<>â ××¦××× ××ª ×¤×¨×××× × ×××¦×× ××¢××
××××× ××ª×× â[]â ××¦××× ××ª ×¤×¨×× ×¨×©××ª.  ×× ××××× ××ª â<>â ×× ××ª
â[]â ××¢×ª ×©××××© ××¤×§××××ª.

××¤×§××××ª ×××××ª ×ª×§×¤××ª:

    ${commands}

×©××××ª ×××©×©××ª ×©×××¨×©×× ××¢× × ×× ××©× ××© ××©××× ××:

    ${administrator} ipython is not available, set use_ipython to no ××××§ ××× ×××ª ××¨×©×××, × ××¨×© ×××©××¨× ×××××ª ××¤×¨×¡×× ×××
××¨×©×××ª ××××××¨:

    ×¨×©×××:    ${listname}
    ×××ª:    ${sender_email}
    × ××©×: ${subject}

××××¢× ×× ××¢××××ª ×××¡×××:

${reasons}

×××× × ××¤× ××, × × ×××§×¨ ×××× ×××§×¨× ×©×× ××× ×××©×¨ ×× ×××××ª
××ª ×××§×©×. × ××¨×© ×××©××¨× ××××©××¨ ××§×©×ª ××¦××¨×¤××ª ××××¨××ª ××¨×©×××ª ××××××¨:


    ×¢×××¨:  ${member}
    ×¨×©×××: ${listname} ×××©××¨× × ××¨×© ××× ×××©×¨ ××ª ××§×©×ª ××××× ×××× ×× ××¨×©×××ª ××××××¨:


    ×××××ª:  ${member}
    ×¨×©×××: ${listname} ×××× ×× ×©× ${member} ××¨×©×××ª ××××××¨ ${listname} ×××©××ª× ××××× ×©× ××§×× ×××××¨× ×××¨×
××Ö¾bounce_score_threshold (×¡×£ × ××§×× ××××¨×) ×©× ××¨×©×××.

××××¢×ª ××¦× ××©×××× (DSN) ×©×××§×¤×¦×ª ××¦××¨×¤×ª ×× ××× ×××× ×. × ××§×× ×××××¨× ×©× ${member} ××¨×©××× ${listname} ×××××.

××××¢×ª ××¦× ××©×××× (DSN) ×©×××§×¤×¦×ª ××¦××¨×¤×ª ×× ××× ×××× ×. ××¨×©××× ${listname} ××© ${count} ××§×©××ª ×¤××§×× ×××ª×× ××ª.

${data}
× × ×××ª××××¡ ××××× ×××§×× ×××¤×©×¨×. ×××× ×× ×©× ${member} ××¨×©×××ª ××××××¨ ${listname} ×××× ×¢×§× ××××¨××ª ×××××××ª. ×××× ×× ×©× ${member} ××¨×©××× ${display_name} ×××× ×××¦×××. ×××××¢× ×××¦××¨×¤×ª ××ª×§××× ×××××¨× ×× ×× ×©×ª×× ××ª ×××××¨× ×× ××××ª× ×× ×©××
××¦×××ª× ××××¥ ××× × ××ª××××ª ×©× ×××¨××.  ×¨×©×××ª ×××××¨ ×× ×××××¨× ××©××× ××ª ×× ××××¢××ª
×××××¨× ×©××× × ××××¢××ª ××× ×××ª ××¨×©×××. ×××¡×¨× ×©× ${member} ××¨×©×××ª ××××××¨ ${display_name} ×××©×××. ××© ××©××× ××§×©××ª ××¨×©×××ª ××××××¨ ${display_name} ××
	${listname}

××× ××××¨×©× ×× ×××× ×¨××©×× ××¨× ××××´×, ××© ××©××× ××××¢× ×¢× âhelpâ
(×¢××¨×) ×× ××©× ×× ×××£ ×××××¢× ××
	${request_email}

× ××ª× ×××××¢ ××× ×××ª ××¨×©××× ×××ª×××ª
	${owner_email}

××©××××ª ××ª×××× × × ××¢×¨×× ××ª ×©××¨×ª ×× ××©× ×©×× ××× ×©×× ×ª××¨×× ×××
â××¢× ×: ××ª××× ×©× ××ª×§×¦××¨ ${display_name}â¦â _______________________________________________
×¨×©×××ª ××××××¨${display_name} â-- ${listname}
××× ×××× ××ª ×××× ×× ×¢××× ××©××× ××××¢×ª ××××´× ×× â${short_listname}-leave@${domain}â ×××ª×××ª ×©×× "${user_email}" ××××× × ×××¦××¨×£ ××¨×©×××ª ××××××¨
 ${short_listname} ×××ª×××ª ${domain} ×¢× ××× ××¢×× ×¨×©×××ª ××××××¨
${short_listname}. × ××ª× ××××¢× ××ª ××××× × ×××¢× × ×××××¢× ××.

×× ×©× ××ª× ×× ××××× ××ª ×××©××¨× ××××ª -- ×××©××¨× ××××ª ××××
×××××¢× ×× ${request_email}:

    confirm ${token}

× × ××©×× ×× ×©×©××××ª `reply' (×ª××××) ×××××¢× ×× ××××¨×
××¢××× ×¢×××¨ ×¨×× ×§××¨×× ×××××´×.

×× ××¢××¤×ª× ××× ××¡×¨× ××××× ×, ××¤×©×¨ ×¤×©×× ×××ª×¢××
××××××¢× ××××ª. ×× ××© ×× ×©××××ª ××¤×©×¨ ××©××× ××××¢×
×× ${owner_email}. ×××©××¨ ×¨××©×× ××ª×××ª ××××´×

×©×××, ××× ×©×¨×ª ×Ö¾Mailman ××××ª GNU ×××ª×××ª ${domain}.

×§×××× × ××ª ××§×©×ª ××¨××©×× ×¢×××¨ ××ª×××ª ×××××´×

    ${user_email}

×××¨× ××ª××¤×©×¨ ×× ×××ª××× ××¢××× ×¢× Mailman ××××ª GNU ×××ª×¨ ××× ×ª××××
×¢××× ×××©×¨ ×©×××ª ××ª×××ª ×××××´× ×©××. × ××ª× ××¢×©××ª ×××ª ×¢× ××× ×©××××ª ×ª××××
×××××¢× ××××ª ×××× ××©× ××ª ××ª ×©××¨×ª ×× ××©× ×××.

×× ××× × ×¨××¦× ××¨×©×× ××ª ××ª×××ª ×××××´× ×××, × ××ª× ×××ª×¢×× ××××××¢×.
×× ×××¢×ª× ××§×©×ª ××¨××©×× ××¨×©××× ×××¦×¢× ×× ×××× ××¨×¦×× × ××××¤× ×××× × ×× ××
××© ×× ×©××××ª ×××©××, × ××ª× ×××¦××¨ ×§×©×¨:

    ${owner_email} ×××©××¨ ××××× ××× ×× ×©× ××ª×××ª ××××´×

×©×××, ××× ×©×¨×ª ×Ö¾GNU Mailman ×××ª×××ª ${domain}.

×§×××× × ××§×©×ª ××××× ××× ×× ×¢×××¨ ××ª×××ª ×××××´×

    ${user_email}

××¤× × ×©×Ö¾GNU Mailman ×ª××× ××¤×©×¨××ª ×××× ××ª ×××× ×× ×©××, ×ª×××× ×¢××× ×××©×¨
××ª ×××§×©× ×©××. × ××ª× ××¢×©××ª ×××ª ×¢× ××× ××¢× × ×××××¢× ×× ×ª×× ×©×××¨× ×¢× ×××ª×¨×ª
×× ××©× ××¤× ×©×××.

×× ××× ××¨×¦×× × ×××× ××ª ×××× ×× ×¢×××¨ ××ª×××ª ×××××´× ××× × ××ª× ×¤×©×× ×××ª×¢××
×××××¢× ××. ×× ×××¢×ª× ×××××¨ ×××××× ××× ×× ×©××§××¨× ×××¦×§× ×××× ××ª ×× ×× ××© ××
×©××××ª × ××¡×¤××ª, × ××ª× ×××¦××¨ ×§×©×¨ ××¨×

    ${owner_email} ××××¢×ª ×××××´× ×©×× ×× â${listname}â ×¢× ×× ××©×

    ${subject}

××¢××××ª ×¢× ×©×××¤× ××¤××§×× ×¢× ××¨×©××× ××××× ××¡×§××¨ ×××¨×××ª ×× × ××ª× ×××©×¨ ×××ª×.

×××××¢× ××¢××××ª ×××¡×××:

${reasons}

×× ×©×××××¢× ×ª×¤××¨×¡× ××¨×©××× ×× ×©×ª××©×× ×××× ××××¢× ×¢× ×××××ª
×××¤×§×××. ×§×××× × ××××¢× ××××ª×××ª ×©×× <${sender_email}> ×¢× ××§×©× ×××©××
××××××× ××¨×©×××ª ××××××¨ ${listname}.

×××¡×¤×¨ ×©×¨××× × ××××: ${count}. ××× ××××× ×¢ ×××¢×××ª ××× ××××××ª ××××´× ××× ×¨××××××
×©× ××××´×, ×× × ×©×× ×¢×× ×ª×××××ª ××××. × × ×× ×¡××ª ×©×× ×××¨.


×× ×××¢×ª× ×××××¨ ××©×××× ×× ×©××© ×× ×©××××ª, × × ×××¦××¨ ×§×©×¨ ×¢× ×× ×××ª ×¨×©×××ª
××××××¨ ×××ª×××ª ${owner_email}. ××××¢×ª× ×¢× ××××ª×¨×ª

    ${subject}

××ª×§××× ×××¦××× ×¢× ××× ×¨×©×××ª ××××××¨ ${display_name}. ×× ××××¢×ª ×ª×©×××. × ××ª× ×××ª×¢×× ××× ×.

×¨×©×××ª ××××××¨ ${listname} ×§×××× ××× ××¡×¤×¨ ×©× ××××¨××ª, ×× ×©××¦××× ×©×××¨×¢×
×ª×§×× ×××¢××¨×ª ××××¢××ª ×× ${sender_email}. ××¦××¨×¤×ª ××××× ××××. × × ×××§××¨ ××ª ×××××¢×
××× ××××× ×©××× ××¢×××ª ×××ª×××ª ×××××´×. ×××× ×××× ×× ××¤× ××ª ××× ×××ª ×××××´× ××¢××¨× × ××¡×¤×ª.

××× ×¦××¨× ××¤×¢××××ª ×××××××ª ××× ××××©××¨ ×××¦× ×××¨××ª ×¤×¢×× ××¨×©×××ª ××××××¨.

×× ××© ×× ×©××××ª ×× ×ª×§×××ª, × ××ª× ×××¦××¨ ×§×©×¨ ×¢× ××¢×× ×¨×©×××ª ××××××¨ ×××ª×××ª

    ${owner_email} ××§×©×ª× ××¨×©×××ª ××××××¨ ${listname}

    ${request}

× ×××ª× ×¢× ××× ××× ×××¤×§×× ××¨×©×××.  ××§×©×ª× × ×××ª×
×¢× ×××¢×¨× ×××× ××¦× ××¤××§××:

â${reason}â

× ××ª× ×××¤× ××ª ×©××××ª ×× ××¢×¨××ª ××©××¨××ª ×× ×× ×××ª ××¨×©×××
×××ª×××ª:

    ${owner_email} ××××¢×ª× ××¨×©×××ª ××××××¨ ${listname} × ×××ª× ×××¡××××ª ×××××ª:

${reasons}


×××××¢× ×××§××¨××ª ×©××ª×§××× ×¢× ××× Mailman ××¦××¨×¤×ª ×××××¢× ××. ×××× ×× ×©×× ××¨×©×××ª ××××××¨ ${listname} ×××©××ª ××××× ×©×§××××ª ××¡×¤×¨ ×××× ×©× ××××¨××ª
×× ×©××¡×× ×©×× ×¨×× ××× ×× ××¢×××ª ×××¢××¨×ª ××××¢××ª ×× ${sender_email}. ×××××¥ ×××××§
×××ª ××× ×× ×××ª ×¡×¤×§ ×××××´× ×©×× ××§×××ª ×¡×××¢ × ××¡×£.


×× ××© ×× ×©××××ª ×× ××¢×××ª, × ××ª× ×××¦××¨ ×§×©×¨ ×¢× ××¢×× ×¨×©×××ª ××××××¨ ××¨×

    ${owner_email} ××¨×× ×××× ××¨×©×××ª ××××××¨ â${display_name}â!

××× ××¤×¨×¡× ××¨×©××× ×××, ×¢××× ××©××× ××ª ×××××¢× ×©×× ××:

  ${listname}

× ××ª× ×××× ××ª ×××× ×× ×× ××©× ××ª ××ª ××¤×©×¨××××ª ×©×× ××¨× ××××´× ×¢× ××× ×©××××ª ××××¢×
××:

  ${request_email}


×¢× ××××× âhelpâ (×¢××¨×) ×× ××©× ×× ××××£ ×××××¢× (××× ××××¨××××ª) ××ª××©×× ×××× ××××¢×
××××¨× ×¢× ×× ××××ª.  ××¦××¨× ×©×× ×× ×××¨×©×××ª ×ª×××¨×© ×× ×¡×¡××, ×× ×××¢×× ×××××
×¡×¡×× ×× ××× × × ××××ª ×××.  ×× ×©×××ª ××ª ××¡×¡×× ×©×× ×××× ×¢××× ×××¤×¡ ×××ª× ××¨×
×× ×©×§ ×××¤××¤×. ×× ×××× ×× ×××× readline not available ××××ª×× ×××××× ×××××× ×××××ª ××¡×¤×¨×× ×©×××× ××××××××: ${value} {} 