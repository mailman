��          �   %   �      P      Q     r     {     �  !   �     �     �          4     R  "   r  !   �     �     �     �           1     R  "   l     �     �     �     �     �       
  8  }  C  �  �     �
  w   �  y     �   �  g   L  u   �  F   *  :   q     �  ,   �  P  �  �   +  1  �  �  �  �  �    �  �  �  a   �  7  �    5  �   F   `  �     >"     	                                                                        
                                                domain:admin:notice:new-list.txt help.txt list:admin:action:post.txt list:admin:action:subscribe.txt list:admin:action:unsubscribe.txt list:admin:notice:disable.txt list:admin:notice:increment.txt list:admin:notice:pending.txt list:admin:notice:removal.txt list:admin:notice:subscribe.txt list:admin:notice:unrecognized.txt list:admin:notice:unsubscribe.txt list:member:digest:masthead.txt list:member:generic:footer.txt list:user:action:invite.txt list:user:action:subscribe.txt list:user:action:unsubscribe.txt list:user:notice:hold.txt list:user:notice:no-more-today.txt list:user:notice:post.txt list:user:notice:probe.txt list:user:notice:refuse.txt list:user:notice:rejected.txt list:user:notice:warning.txt list:user:notice:welcome.txt Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Automatically generated
Language-Team: none
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 The mailing list '$listname' has just been created for you.  The
following is some basic information about your mailing list.

There is an email-based interface for users (not administrators) of your list;
you can get info about using it by sending a message with just the word 'help'
as subject or in the body, to:

    $request_email

Please address all questions to $site_email. Help for $listname mailing list

This is email command 'help' for version $version of the GNU Mailman list
manager at $domain.  The following describes commands you can send to get
information about and control your subscription to Mailman lists at this site.
A command can be in the Subject line or in the body of the message.

Commands should be sent to the ${listname}-request@${domain} address.

About the descriptions - words in "<>"s signify REQUIRED items and
words in "[]" denote OPTIONAL items.  Do not include the "<>"s or
"[]"s when you use the commands.

The following commands are valid:

    $commands

Questions and concerns for the attention of a person should be sent to:

    $administrator As list administrator, your authorization is requested for the
following mailing list posting:

    List:    $listname
    From:    $sender_email
    Subject: $subject

The message is being held because:

$reasons

At your convenience, visit your dashboard to approve or deny the
request. Your authorization is required for a mailing list subscription request
approval:

    For:  $member
    List: $listname Your authorization is required for a mailing list unsubscription
request approval:

    For:  $member
    List: $listname $member's subscription has been disabled on $listname due to their bounce score
exceeding the mailing list's bounce_score_threshold.

The triggering DSN if available is attached. $member's bounce score on $listname has been incremented.

The triggering DSN if available is attached. The $listname list has $count moderation requests waiting.

$data
Please attend to this at your earliest convenience. $member has been unsubscribed from $listname due to excessive bounces. $member has been successfully subscribed to $display_name. The attached message was received as a bounce, but either the bounce format
was not recognized, or no member addresses could be extracted from it.  This
mailing list has been configured to send all unrecognized bounce messages to
the list administrator(s). $member has been removed from $display_name. Send $display_name mailing list submissions to
	$listname

To subscribe or unsubscribe via email, send a message with subject or body
'help' to
	$request_email

You can reach the person managing the list at
	$owner_email

When replying, please edit your Subject line so it is more specific than
"Re: Contents of $display_name digest..." _______________________________________________
$display_name mailing list -- $listname
To unsubscribe send an email to ${short_listname}-leave@${domain} Your address "$user_email" has been invited to join the $short_listname
mailing list at $domain by the $short_listname mailing list owner.
You may accept the invitation by simply replying to this message.

Or you should include the following line -- and only the following
line -- in a message to $request_email:

    confirm $token

Note that simply sending a `reply' to this message should work from
most mail readers.

If you want to decline this invitation, please simply disregard this
message.  If you have any questions, please send them to
$owner_email. Email Address Registration Confirmation

Hello, this is the GNU Mailman server at $domain.

We have received a registration request for the email address

    $user_email

Before you can start using GNU Mailman at this site, you must first confirm
that this is your email address.  You can do this by replying to this message.

Or you should include the following line -- and only the following
line -- in a message to $request_email:

    confirm $token

Note that simply sending a `reply' to this message should work from
most mail readers.

If you do not wish to register this email address, simply disregard this
message.  If you think you are being maliciously subscribed to the list, or
have any other questions, you may contact

    $owner_email Email Address Unsubscription Confirmation

Hello, this is the GNU Mailman server at $domain.

We have received an unsubscription request for the email address

    $user_email

Before GNU Mailman can unsubscribe you, you must first confirm your request.
You can do this by simply replying to this message.

Or you should include the following line -- and only the following
line -- in a message to $request_email:

    confirm $token

Note that simply sending a `reply' to this message should work from
most mail readers.

If you do not wish to unsubscribe this email address, simply disregard this
message.  If you think you are being maliciously unsubscribed from the list,
or have any other questions, you may contact

    $owner_email Your mail to '$listname' with the subject

    $subject

Is being held until the list moderator can review it for approval.

The message is being held because:

$reasons

Either the message will get posted to the list, or you will receive
notification of the moderator's decision. We have received a message from your address <$sender_email> requesting an
automated response from the $listname mailing list.

The number we have seen today: $count.  In order to avoid problems such as
mail loops between email robots, we will not be sending you any further
responses today.  Please try again tomorrow.

If you believe this message is in error, or if you have any questions, please
contact the list owner at $owner_email. Your message entitled

    $subject

was successfully received by the $display_name mailing list. This is a probe message.  You can ignore this message.

The $listname mailing list has received a number of bounces from you,
indicating that there may be a problem delivering messages to $sender_email.  A
sample is attached below.  Please examine this message to make sure there are
no problems with your email address.  You may want to check with your mail
administrator for more help.

You don't need to do anything to remain an enabled member of the mailing list.

If you have any questions or problems, you can contact the mailing list owner
at

    $owner_email Your request to the $listname mailing list

    $request

has been rejected by the list moderator.  The moderator gave the
following reason for rejecting your request:

"$reason"

Any questions or comments should be directed to the list administrator
at:

    $owner_email Your message to the $listname mailing-list was rejected for the following
reasons:

$reasons

The original message as received by Mailman is attached. Your subscription has been disabled on the $listname mailing list because it has
received a number of bounces indicating that there may be a problem delivering
messages to $sender_email. You may want to check with your mail administrator
for more help.

If you have any questions or problems, you can contact the mailing list owner at

    $owner_email Welcome to the "$display_name" mailing list!

To post to this list, send your message to:

  $listname

You can unsubscribe or make adjustments to your options via email by sending
a message to:

  $request_email

with the word 'help' in the subject or body (don't include the quotes), and
you will get back a message with instructions.  You will need your password to
change your options, but for security purposes, this password is not included
here.  If you have forgotten your password you will need to reset it via the
web UI. 