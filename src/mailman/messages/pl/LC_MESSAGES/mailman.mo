��    �       -  �      �"  �  �"  a  �$     �&     '     '     '     +'     <'     R'  =   j'  �   �'  T   *(  �  (  �   C*    +  ;    -  �   \-  c   �-  �   X.  �   /     0  }   %0  �   �0  '   �1  �   �1  �   _2  Y   3  �   m3  �   84  �   /5  �   �5  �  �6  p   �8  *   9  g   >9  ^   �9  \   :  $  b:  Y   �;  o   �;  s   Q<  _   �<  G   %=     m=  q  �=  �  �>  �  �@  C   �B  �   �B  �   �C  �   7D  �   �D  �   PE    �E     �I  y   J  �  �J  P   pM  I   �M  �   N  �   �N  �   �O  :   �P  P   �P  J   2Q  9   }Q  �   �Q  �   �R  �   \S  {   �S  b   \T  ^   �T  R   U  �   qU  �   =V  g   +W  >  �W     �X  �   �X  �  �Y  q   >\     �\  �   �\  I   �]  �   �]  t   �^  J   _  �   \_  o   Q`  w   �`  �   9a     �a  &   b  #   )b  <   Mb  8   �b  M   �b  =   c  :   Oc  C   �c  0   �c  /   �c  2   /d  1   bd  @   �d  U   �d     +e  Q   Ke  P   �e     �e  4   f  %   =f  %   cf  G   �f  J   �f  K   g  G   hg  @   �g  1   �g  9   #h  .   ]h  8   �h     �h     �h  (   �h  3   i  #   Ki  �   oi  K   �i     Hj     ej  7   wj      �j      �j  3   �j     %k  (   Dk  (   mk     �k  0   �k  .   �k  '   l     4l     Hl     ]l  <   sl  �   �l  !   zm     �m  �  �m  D   �o     �o  -   �o  0   p  *   Ep  7   pp  6   �p  8   �p  8   q  )   Qq  2   {q  '   �q  1   �q  #   r  *   ,r  /   Wr  $   �r  D   �r      �r  	   s  #   s  ,   @s     ms  ,   ~s     �s     �s  &   �s  4   �s     #t  3   >t     rt  	   �t  /   �t     �t  $   �t  "   �t  |   u     �u     �u  C   �u     v  +   .v  *   Zv     �v     �v  (   �v  $   �v  8   w  1   Fw  !   xw    �w  #   �y  m  �y     D{  "   c{  #   �{  #   �{  (   �{  9   �{     1|     L|  '   l|  -   �|  '   �|     �|     }  (   }     G}  C   `}     �}     �}  %   �}  :   �}  )   ,~     V~     _~     x~  d   �~  (   �~  )   &  "   P  %   s  *   �  "   �  )   �     �     $�     C�  ,   `�      ��  #   ��     Ҁ  !   �  *   �     6�     T�     e�  ,   }�     ��     ��  Q   ��  A   �  S   R�     ��     ��     ܂  .   ��     +�  #   A�      e�     ��  '   ��     ʃ     �     ��  4   �     H�     b�  2   p�  8   ��     ܄  '   ��     �  %   )�  4   O�  =   ��  1     C   �  %   8�  +   ^�  %   ��     ��     Æ      �  +   �  #  .�  y  R�     ̉  0   ى  !   
�  ,   ,�     Y�  1   t�     ��     ��  2   ފ     �  $   0�     U�  '   o�     ��  2   ��     ֋     ��  6   �      F�  %   g�  8   ��     ƌ  .   ތ      �  1   .�     `�  -   z�     ��  1   ��     �  -   ��     *�      C�     d�  �   y�  "   `�  "   ��     ��     ŏ     �  }   �  w   �  e   ��    ]�  �   u�  )   �  6   ,�      c�  1   ��  %   ��  4   ܓ  5   �  6   G�  7   ~�  6   ��  "   �  &   �  0   7�     h�  /   ��  G  ��  V   ��  "   R�     u�     ��     ��  &   ��  '   �     �     "�  	   9�  =   C�    ��  2   ��  �   ̙  L   ��  F   �  u  3�  R   ��  S   ��  @   P�  '   ��  J   ��  �   �     ��     ��     О     �     ��  !   ��     �     :�     L�     _�      y�     ��  /   ��     ӟ     �  !   �     0�     N�     n�     ��     ��  "   ʠ  !   ��     �     /�     N�     j�      ��     ��  "   ġ     �     �     �     8�     V�     s�     ��     ��     ��  *   ��     �  �  �     ��  �  ٦     ��     ��  
   ��     ʩ     ީ     ��  &   �  D   9�  �   ~�  m   �    u�  �   }�  b  u�  E   ذ  �   �  x   ̱  �   E�    :�     V�  �   h�    �  ,   �  �   G�  �   �  m   Ʒ  �   4�    '�  �   B�  �   �  �  ݻ  �   ��  7   �  w   M�  c   ſ  i   )�  >  ��  z   ��  �   M�  �   ��  x   ^�  }   ��     U�  �  f�  �  �  �  �  X   �  �   d�  �   �  �   ��  �   b�  �   �  �  ��     p�  �   ��  ]  "�  a   ��  a   ��  �   D�  �   !�  �   �  :   ��  X   5�  Q   ��  9   ��  �   �  �   �  �   �  �   ��  t   %�  m   ��  S   �    \�  .  ^�  l   ��  P  ��     K�  �   k�  �  -�  t   �     ��  �   ��  `   ��  �   ��  �   ��  T   Q�    ��  �   ��  �   T�  �   ��     ��  2   ��  #   �  G   0�  F   x�  U   ��  R   �  S   h�  I   ��  ,   �  1   3�  :   e�  >   ��  A   ��  \   !�  &   ~�  ]   ��  T   �     X�  6   w�  *   ��  '   ��  J   �  K   L�  P   ��  N   ��  D   8�  @   }�  R   ��  5   �  <   G�     ��  &   ��  (   ��  7   ��  (   �  �   C�  a   ��     6�     U�  E   k�  !   ��     ��  6   ��  #   )�  /   M�  .   }�     ��  4   ��  2   ��  (   .�     W�     l�     ~�  B   ��  �   ��  %   ��     ��    �  M   !�  $   o�  %   ��  /   ��  8   ��  J   #�  Q   n�  B   ��  >   �  2   B�  C   u�  3   ��  A   ��  6   /  :   f  >   �  9   �  ]       x    � -   � 4   �     -       E    S /   e ,   �    � >   � "    	   A ,   K    x )   � 6   � �   � #   k !   � F   �     � 1    8   K    � $   � 6   � &   � 5   $ <   Z 0   � Q  � 4   	 �  O	 #   �
 (   �
 ,    .   K %   z <   � $   � %    ,   ( :   U 2   �    � "   � :       > [   ^    � #   � %   � F    *   _ 	   �    � *   � �   � ?   n H   � D   � 4   < .   q %   � 2   �    � .       ? 5   [ (   � )   �    � '   � 5   &    \    y "   � F   �    �    � O    P   \ \   � *   
 '   5 ,   ] >   �    � %   � '   
 $   2 2   W    �    �    � =   �    !    @ H   Q ;   �    � #   �     +   ) O   U [   � '    \   ) 3   � 8   � .   � $   " (   G )   p <   � :  � �      � .   � !   � 9   �    6 /   S    � %   � E   � )    /   6 !   f .   �    � G   �        ( 7   D (   | &   � O   �      *   <  &   g  8   �  !   �  0   �  
   ! A   %!    g! 4   {! "   �! $   �!    �! �   	" -   �" ,   "#    O#     n#    �# v   �# �   $$ o   �$ *  *% �   U& 1   �& 9   )' *   c' 7   �' .   �' 4   �' 5   *( 8   `( 9   �( 1   �(    ) F   #) +   j)    �) 1   �) j  �) J   M+ (   �+    �+ !   �+ '   �+ )   , .   I, '   x, #   �,    �, C   �, C  - 9   ]. �   �. L   }/ =   �/ �  0 V   �1 R   �1 V   J2 -   �2 A   �2 �   3    �3    �3    �3 
   4 
   %4 &   04 !   W4    y4    �4    �4 �  �4 �  x6 3   V9 U  �9 �   �: �   c; �   �; �   �< m   �= [   �= O   U> /  �> *   �? �   @ �   �A �  hB >  �D L  9H #  �K 	  �L m   �N �  "O   �Q �   �R �  bS u  U    }W    �W    �W 9   �W    �W        &  r   �   D  a   �  �        �                   �   �   �   w  8  y       �   �   E      �        [   �       @      V   �       k   �   S     V  )   .   F   �   �       0  �   �   y  �       �   �  L   \   �       �     �  i  c    K   �               �   �       4   �   {        =                 b   �  �   �   M   @   �       �   f  \  H   �     �   b      ^   *           c   �       �   �   �   �           %       �   �  �       t   �      �       I   �   ~  6   7  �  �   �   N      ,  �           +   �   �  g  �                 O                  �   �           0   �           k  �   �          -   ?      Y   �       :  R   l   d   s   '  �      }   T       X   5   P      �           �       I  '                   �   F  `      4  �          x           �   `  i       �   m  �   <   �             �       �   �       d          �          9       �      1  �  ?               q  "  }  �   j  e       $  Q   ~                 C  n  �      �  ]       	   {   l         �   J    M  �   p   �     �   �   �       �     $   �          �   !   n   o   �          C   �      �   �   _  =   A   !            �      �   �     �  �   �       r  �   ]  |       �   G  8       X  �   O  /       x  W   �   �   �   �  2  �      .  ,   
   j   �          [  �       e  v              ;  B     Z   |          E   9   �       m           u  :   K          &       G   q   �              �   u       �      #             Z  �             �  �   6  (     L      +             	  �  �     5  "   �   (  J   �   z   %  B   /     �   �   _   �   
           �       R      s        H         �             ;   �   #      S   �   �   2   �   N   �  �   z      3   h       �           �      �             W  >  �         1     �   g   �   U  t  �   �   3              �   7       �   �   �      �       f      �   a  �   *  �  p  �   A  <  P             �  o  �   Q  U   >   �  ^      w   -              �          �   �   �  D               �  �   v  h  �   )  �   T  �              �   �           Y       
    Run a script.  The argument is the module path to a callable.  This
    callable will be imported and then, if --listspec/-l is also given, is
    called with the mailing list as the first argument.  If additional
    arguments are given at the end of the command line, they are passed as
    subsequent positional arguments to the callable.  For additional help, see
    --details.

    If no --listspec/-l argument is given, the script function being called is
    called with no arguments.
     
    Start the named runner, which must be one of the strings returned by the -l
    option.

    For runners that manage a queue directory, optional `slice:range` if given
    is used to assign multiple runner processes to that queue.  range is the
    total number of runners for the queue while slice is the number of this
    runner from [0..range).  For runners that do not manage a queue, slice and
    range are ignored.

    When using the `slice:range` form, you must ensure that each runner for the
    queue is given the same range value.  If `slice:runner` is not given, then
    1:1 is used.
     
- Done. 
- Ignored: 
- Results: 
- Unprocessed: 
Held Messages:
 
Held Subscriptions:
 
Held Unsubscriptions:
 
Replaced multipart/alternative part with first alternative.
 
___________________________________________
Mailman's content filtering has removed the
following MIME parts from this message.
     A more verbose output including the file system paths that Mailman is
    using.     A specification of the mailing list to operate on.  This may be the posting
    address of the list, or its List-ID.  The argument can also be a Python
    regular expression, in which case it is matched against both the posting
    address and List-ID of all mailing lists.  To use a regular expression,
    LISTSPEC must start with a ^ (and the matching is done with re.match().
    LISTSPEC cannot be a regular expression unless --run is given.     Add all member addresses in FILENAME with delivery mode as specified
    with -d/--delivery.  FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
         Add and delete members as necessary to syncronize a list's membership
    with an input file.  FILENAME is the file containing the new membership,
    one member per line.  Blank lines and lines that start with a '#' are
    ignored.  Addresses in FILENAME which are not current list members
    will be added to the list with delivery mode as specified with
    -d/--delivery.  List members whose addresses are not in FILENAME will
    be removed from the list.  FILENAME can be '-' to indicate standard input.
         Add and/or delete owners and moderators of a list.
         Additional metadata key/value pairs to add to the message metadata
    dictionary.  Use the format key=value.  Multiple -m options are
    allowed.     Change a user's email address from old_address to possibly case-preserved
    new_address.
         Configuration file to use.  If not given, the environment variable
    MAILMAN_CONFIG_FILE is consulted and used if set.  If neither are given, a
    default configuration file is loaded.     Create a mailing list.

    The 'fully qualified list name', i.e. the posting address of the mailing
    list is required.  It must be a valid email address and the domain must be
    registered with Mailman.  List names are forced to lower case.     Date: ${date}     Delete all the members of the list.  If specified, none of -f/--file,
    -m/--member or --fromall may be specified.
         Delete list members whose addresses are in FILENAME in addition to those
    specified with -m/--member if any.  FILENAME can be '-' to indicate
    standard input.  Blank lines and lines that start with a '#' are ignored.
         Delete members from a mailing list.     Delete the list member whose address is ADDRESS in addition to those
    specified with -f/--file if any.  This option may be repeated for
    multiple addresses.
         Delete the member(s) specified by -m/--member and/or -f/--file from all
    lists in the installation.  This may not be specified together with
    -a/--all or -l/--list.
         Discard all shunted messages instead of moving them back to their original
    queue.     Display a mailing list's members.
    Filtering along various criteria can be done when displaying.
    With no options given, displaying mailing list members
    to stdout is the default mode.
         Display all memberships for a user or users with address matching a
    pattern. Because part of the process involves converting the pattern
    to a SQL query with wildcards, the pattern should be simple. A simple
    string works best.
         Display only digest members of kind.
    'any' means any digest type, 'plaintext' means only plain text (rfc 1153)
    type digests, 'mime' means MIME type digests.     Display only members with a given ROLE.
    The role may be 'any', 'member', 'nonmember', 'owner', 'moderator',
    or 'administrator' (i.e. owners and moderators).
    If not given, then 'member' role is assumed.     Display only members with a given delivery status.
    'enabled' means all members whose delivery is enabled, 'any' means
    members whose delivery is disabled for any reason, 'byuser' means
    that the member disabled their own delivery, 'bybounces' means that
    delivery was disabled by the automated bounce processor,
    'byadmin' means delivery was disabled by the list
    administrator or moderator, and 'unknown' means that delivery was disabled
    for unknown (legacy) reasons.     Display only memberships with the given role.  If not given, 'all' role,
    i.e. all roles, is the default.     Display only regular delivery members.     Display output to FILENAME instead of stdout.  FILENAME
    can be '-' to indicate standard output.     Don't actually do anything, but in conjunction with --verbose, show what
    would happen.     Don't actually make the changes.  Instead, print out what would be
    done to the list.     Don't attempt to pretty print the object.  This is useful if there is some
    problem with the object and you just want to get an unpickled
    representation.  Useful with 'mailman qfile -i <file>'.  In that case, the
    list of unpickled objects will be left in a variable called 'm'.     Don't print status messages.  Error messages are still printed to standard
    error.     Don't restart the runners when they exit because of an error or a SIGUSR1.
    Use this only for debugging.     Email address of the user to be removed from the given role.
    May be repeated to delete multiple users.
         File to send the output to.  If not given, or if '-' is given, standard
    output is used.     File to send the output to.  If not given, standard output is used.     From: ${from_}     Generate the MTA alias files upon startup. Some MTA, like postfix, can't
    deliver email if alias files mentioned in its configuration are not
    present. In some situations, this could lead to a deadlock at the first
    start of mailman3 server. Setting this option to true will make this
    script create the files and thus allow the MTA to operate smoothly.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option, the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option,the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     If you are sure you want to run as root, specify --run-as-root.     Import Mailman 2.1 list data.  Requires the fully-qualified name of the
    list to import and the path to the Mailman 2.1 pickle file.     Increment the digest volume number and reset the digest number to one.  If
    given with --send, the volume number is incremented before any current
    digests are sent.     Key to use for the lookup.  If no section is given, all the key-values pair
    from any section matching the given key will be displayed.     Leaves you at an interactive prompt after all other processing is complete.
    This is the default unless the --run option is given.     List only those mailing lists hosted on the given domain, which
    must be the email host name.  Multiple -d options may be given.
         Master subprocess watcher.

    Start and watch the configured runners, ensuring that they stay alive and
    kicking.  Each runner is forked and exec'd in turn, with the master waiting
    on their process ids.  When it detects a child runner has exited, it may
    restart it.

    The runners respond to SIGINT, SIGTERM, SIGUSR1 and SIGHUP.  SIGINT,
    SIGTERM and SIGUSR1 all cause a runner to exit cleanly.  The master will
    restart runners that have exited due to a SIGUSR1 or some kind of other
    exit condition (say because of an uncaught exception).  SIGHUP causes the
    master and the runners to close their log files, and reopen then upon the
    next printed message.

    The master also responds to SIGINT, SIGTERM, SIGUSR1 and SIGHUP, which it
    simply passes on to the runners.  Note that the master will close and
    reopen its own log files on receipt of a SIGHUP.  The master also leaves
    its own process id in the file specified in the configuration file but you
    normally don't need to use this PID directly.     Message-ID: ${message_id}     Name of file containing the message to inject.  If not given, or
    '-' (without the quotes) standard input is used.     Normally, this script will refuse to run if the user id and group id are
    not set to the 'mailman' user and group (as defined when you configured
    Mailman).  If run as root, this script will change to this user and group
    before the check is made.

    This can be inconvenient for testing and debugging purposes, so the -u flag
    means that the step that sets and checks the uid/gid is skipped, and the
    program is run as the current user and group.  This flag is not recommended
    for normal production environments.

    Note though, that if you run with -u and are not in the mailman group, you
    may have permission problems, such as being unable to delete a list's
    archives through the web.  Tough luck!     Notify the list owner by email that their mailing list has been
    created.     Operate on a mailing list.

    For detailed help, see --details
         Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the digests for all mailing lists.     Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the requests for all mailing lists.     Override the default set of runners that the master will invoke, which is
    typically defined in the configuration file.  Multiple -r options may be
    given.  The values for -r are passed straight through to bin/runner.     Override the list's setting for admin_notify_mchanges.     Override the list's setting for send_goodbye_message to
    deleted members.     Override the list's setting for send_welcome_message to added members.     Override the list's setting for send_welcome_message.     Register the mailing list's domain if not yet registered.  This is
    the default behavior, but these options are provided for backward
    compatibility.  With -D do not register the mailing list's domain.     Run the named runner exactly once through its main loop.  Otherwise, the
    runner runs indefinitely until the process receives a signal.  This is not
    compatible with runners that cannot be run once.     Running mailman commands as root is not recommended and mailman will
    refuse to run as root unless this option is specified.     Section to use for the lookup.  If no key is given, all the key-value pairs
    of the given section will be displayed.     Send any collected digests for the List only if their digest_send_periodic
    is set to True.     Send any collected digests right now, even if the size threshold has not
    yet been met.     Send the added members an invitation rather than immediately adding them.
         Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.     Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.  Ignored for invited
    members.     Set the list's preferred language to CODE, which must be a registered two
    letter language code.     Specify a list owner email address.  If the address is not currently
    registered with Mailman, the address is registered and linked to a user.
    Mailman will send a confirmation message to the address, but it will also
    send a list creation notice to the address.  More than one owner can be
    specified.     Specify a list style name.     Specify the encoding of strings in PICKLE_FILE if not utf-8 or a subset
    thereof. This will normally be the Mailman 2.1 charset of the list's
    preferred_language.     Start a runner.

    The runner named on the command line is started, and it can either run
    through its main loop once (for those runners that support this) or
    continuously.  The latter is how the master runner starts all its
    subprocesses.

    -r is required unless -l or -h is given, and its argument must be one of
    the names displayed by the -l switch.

    Normally, this script should be started from `mailman start`.  Running it
    separately or with -o is generally useful only for debugging.  When run
    this way, the environment variable $MAILMAN_UNDER_MASTER_CONTROL will be
    set which subtly changes some error handling behavior.
         Start an interactive Python session, with a variable called 'm'
    containing the list of unpickled objects.     Subject: ${subject}     The gatenews command is run periodically by the nntp runner.
    If you are running it via cron, you should remove it from the crontab.
    If you want to run it manually, set _MAILMAN_GATENEWS_NNTP in the
    environment.     The list to operate on.  Required unless --fromall is specified.
         The name of the queue to inject the message to.  QUEUE must be one of the
    directories inside the queue directory.  If omitted, the incoming queue is
    used.     The role to add/delete. This may be 'owner' or 'moderator'.
    If not given, then 'owner' role is assumed.
         This option has no effect. It exists for backwards compatibility only.     User to add with the given role. This may be an email address or, if
    quoted, any display name and email address parseable by
    email.utils.parseaddr. E.g., 'Jane Doe <jane@example.com>'. May be repeated
    to add multiple users.
         [MODE] Add all member addresses in FILENAME.  This option is removed.
    Use 'mailman addmembers' instead.     [MODE] Delete all member addresses found in FILENAME.
    This option is removed. Use 'mailman delmembers' instead.     [MODE] Synchronize all member addresses of the specified mailing list
    with the member addresses found in FILENAME.
    This option is removed. Use 'mailman syncmembers' instead.  (Digest mode) ${count} matching mailing lists found: ${display_name} post acknowledgment ${email} is already a ${role.name} of ${mlist.fqdn_listname} ${email} is not a ${role.name} of ${mlist.fqdn_listname} ${member} unsubscribed from ${mlist.display_name} mailing list due to bounces ${member}'s bounce score incremented on ${mlist.display_name} ${member}'s subscription disabled on ${mlist.display_name} ${mlist.display_name} Digest, Vol ${volume}, Issue ${digest_number} ${mlist.display_name} mailing list probe message ${mlist.display_name} subscription notification ${mlist.display_name} uncaught bounce notification ${mlist.display_name} unsubscription notification ${mlist.fqdn_listname} post from ${msg.sender} requires approval ${mlist.list_id} bumped to volume ${mlist.volume}, number ${mlist.next_digest_number} ${mlist.list_id} has no members ${mlist.list_id} is at volume ${mlist.volume}, number ${mlist.next_digest_number} ${mlist.list_id} sent volume ${mlist.volume}, number ${mlist.next_digest_number} ${name} runs ${classname} ${person} has a pending subscription for ${listname} ${person} left ${mlist.fqdn_listname} ${realname} via ${mlist.display_name} ${self.email} is already a member of mailing list ${self.fqdn_listname} ${self.email} is already a moderator of mailing list ${self.fqdn_listname} ${self.email} is already a non-member of mailing list ${self.fqdn_listname} ${self.email} is already an owner of mailing list ${self.fqdn_listname} ${self.name}: ${email} is not a member of ${mlist.fqdn_listname} ${self.name}: No valid address found to subscribe ${self.name}: No valid email address found to unsubscribe ${self.name}: no such command: ${command_name} ${self.name}: too many arguments: ${printable_arguments} (no subject) - Original message details: -------------- next part --------------
 --send and --periodic flags cannot be used together <----- start object ${count} -----> A message part incompatible with plain text digests has been removed ...
Name: ${filename}
Type: ${ctype}
Size: ${size} bytes
Desc: ${desc}
 A previous run of GNU Mailman did not exit cleanly ({}).  Try using --force A rule which always matches. Accept a message. Add a list-specific prefix to the Subject header value. Add the RFC 2369 List-* headers. Add the message to the archives. Add the message to the digest, possibly sending it. Address changed from {} to {}. Address {} already exists; can't change. Address {} is not a valid email address. Address {} not found. Addresses are not different.  Nothing to change. After content filtering, the message was empty Already subscribed (skipping): ${email} An alias for 'end'. An alias for 'join'. An alias for 'leave'. An alternative directory to output the various MTA files to. And you can print the list's request address by running:

% mailman withlist -r listaddr.requestaddr -l ant@example.com
Importing listaddr ...
Running listaddr.requestaddr() ...
ant-request@example.com Announce only mailing list style. Apply DMARC mitigations. As another example, say you wanted to change the display name for a particular
mailing list.  You could put the following function in a file called
`change.py`:

def change(mlist, display_name):
    mlist.display_name = display_name

and run this from the command line:

% mailman withlist -r change -l ant@example.com 'My List'

Note that you do not have to explicitly commit any database transactions, as
Mailman will do this for you (assuming no errors occured). Auto-response for your message to the "${display_name}" mailing list Bad runner spec: ${value} Calculate the owner and moderator recipients. Calculate the regular recipients of the message. Cannot import runner module: ${class_path} Cannot parse as valid email address (skipping): ${line} Cannot unshunt message ${filebase}, skipping:
${error} Catch messages that are bigger than a specified maximum. Catch messages with digest Subject or boilerplate quote. Catch messages with implicit destination. Catch messages with no, or empty, Subject headers. Catch messages with suspicious headers. Catch messages with too many explicit recipients. Catch mis-addressed email commands. Cleanse certain headers from all messages. Confirm a subscription or held message request. Confirmation email sent to ${person} Confirmation email sent to ${person} to leave ${mlist.fqdn_listname} Confirmation token did not match Confirmed Content filter message notification Created mailing list: ${mlist.fqdn_listname} DMARC moderation Decorate a message with headers and footers. Digest Footer Digest Header Discard a message and stop processing. Discussion mailing list style with private archives. Display Mailman's version. Display more debugging information to the log file. Echo back your arguments. Email: {} Emergency moderation is in effect for this list End of  Filter the MIME content of messages. Find DMARC policy of From: domain. For unknown reasons, the master lock could not be acquired.

Lock file: ${config.LOCK_FILE}
Lock host: ${hostname}

Exiting. Forward of moderated message GNU Mailman is already running GNU Mailman is in an unexpected state (${hostname} != ${fqdn_name}) GNU Mailman is not running GNU Mailman is running (master pid: ${pid}) GNU Mailman is stopped (stale pid: ${pid}) Generating MTA alias maps Get a list of the list members. Get help about available email commands. Get information out of a queue file. Get the normal delivery recipients from an include file. Header "{}" matched a bounce_matching_header line Header "{}" matched a header rule Here's an example of how to use the --run option.  Say you have a file in the
Mailman installation directory called 'listaddr.py', with the following two
functions:

def listaddr(mlist):
    print(mlist.posting_address)

def requestaddr(mlist):
    print(mlist.request_address)

Run methods take at least one argument, the mailing list object to operate
on.  Any additional arguments given on the command line are passed as
positional arguments to the callable.

If -l is not given then you can run a function that takes no arguments.
 Hold a message and stop processing. If you reply to this message, keeping the Subject: header intact, Mailman will
discard the held message.  Do this if the message is spam.  If you reply to
this message and include an Approved: header with the list password in it, the
message will be approved for posting to the list.  The Approved: header can
also appear in the first line of the body of the reply. Ignoring non-dictionary: {0!r} Ignoring non-text/plain MIME parts Illegal list name: ${fqdn_listname} Illegal owner addresses: ${invalid} Information about this Mailman instance. Inject a message from a file into a mailing list's queue. Invalid Approved: password Invalid email address: ${email} Invalid language code: ${language_code} Invalid or unverified email address: ${email} Invalid value for [shell]use_python: {} Is the master even running? Join this mailing list. Last autoresponse notification for today Leave this mailing list. Leave this mailing list.

You may be asked to confirm your request. Less verbosity List all mailing lists. List already exists: ${fqdn_listname} List only those mailing lists that are publicly advertised List the available runner names and exit. List: {} Look for a posting loop. Look for any previous rule hit. Match all messages posted to a mailing list that gateways to a
        moderated newsgroup.
         Match messages sent by banned addresses. Match messages sent by moderated members. Match messages sent by nonmembers. Match messages with no valid senders. Member not subscribed (skipping): ${email} Members of the {} mailing list:
{} Membership is banned (skipping): ${email} Message ${message} Message contains administrivia Message has a digest subject Message has already been posted to this list Message has implicit destination Message has more than {} recipients Message has no subject Message quotes digest boilerplate Message sender {} is banned from this list Missing data for request {}

 Moderation chain Modify message headers. Move the message to the outgoing news queue. N/A Name: ${fname}
 New subscription request to ${self.mlist.display_name} from ${self.address.email} New unsubscription request from ${mlist.display_name} by ${email} New unsubscription request to ${self.mlist.display_name} from ${self.address.email} No child with pid: ${pid} No confirmation token found No matching mailing lists found No registered user for email address: ${email} No runner name given. No sender was found in the message. No such command: ${command_name} No such list found: ${spec} No such list matching spec: ${listspec} No such list: ${_list} No such list: ${listspec} No such queue: ${queue} Not a Mailman 2.1 configuration file: ${pickle_file} Nothing to add or delete. Nothing to do Notify list owners/moderators of pending requests. Number of objects found (see the variable 'm'): ${count} Operate on digests. Ordinary discussion mailing list style. Original Message PID unreadable in: ${config.PID_FILE} Perform ARC auth checks and attach resulting headers Perform auth checks and attach Authentication-Results header. Perform some bookkeeping after a successful post. Poll the NNTP server for messages to be gatewayed to mailing lists. Post to a moderated newsgroup gateway Posting of your message titled "${subject}" Print detailed instructions and exit. Print less output. Print some additional status. Print the Mailman configuration. Process DMARC reject or discard mitigations Produces a list of member names and email addresses.

The optional delivery= and mode= arguments can be used to limit the report
to those members with matching delivery status and/or delivery mode.  If
either delivery= or mode= is specified more than once, only the last occurrence
is used.
 Programmatically, you can write a function to operate on a mailing list, and
this script will take care of the housekeeping (see below for examples).  In
that case, the general usage syntax is:

% mailman withlist [options] -l listspec [args ...]

where `listspec` is either the posting address of the mailing list
(e.g. ant@example.com), or the List-ID (e.g. ant.example.com). Reason: {}

 Regenerate the aliases appropriate for your MTA. Regular expression requires --run Reject/bounce a message and stop processing. Remove DomainKeys headers. Remove a mailing list (does not remove archives). Removed list: ${listspec} Reopening the Mailman runners Request to mailing list "${display_name}" rejected Restarting the Mailman runners Send an acknowledgment of a posting. Send automatic responses. Send the message to the outgoing queue. Sender: {}
 Show a list of all available queue names and exit. Show also the list descriptions Show also the list names Show the current running status of the Mailman system. Show this help message and exit. Shutting down Mailman's master runner Signal the Mailman processes to re-open their log files. Stale pid file removed. Start the Mailman master and runner processes. Starting Mailman's master runner Stop and restart the Mailman runner subprocesses. Stop processing commands. Stop the Mailman master and runner processes. Subject: {}
 Subscription already pending (skipping): ${email} Subscription request Suppress some duplicates of the same message. Suppress status messages Tag messages with topic matches. The Mailman Replybot The attached message matched the ${mlist.display_name} mailing list's content
filtering rules and was prevented from being forwarded on to the list
membership.  You are receiving the only remaining copy of the discarded
message.

 The built-in -owner posting chain. The built-in header matching chain The built-in moderation chain. The built-in owner pipeline. The built-in posting pipeline. The content of this message was lost. It was probably cross-posted to
multiple lists and previously handled on another list.
 The mailing list is in emergency hold and this message was not
        pre-approved by the list administrator.
         The master lock could not be acquired because it appears as though another
master is already running. The master lock could not be acquired, because it appears as if some process
on some other host may have acquired it.  We can't test for stale locks across
host boundaries, so you'll have to clean this up manually.

Lock file: ${config.LOCK_FILE}
Lock host: ${hostname}

Exiting. The master lock could not be acquired.  It appears as though there is a stale
master lock.  Try re-running ${program} with the --force flag. The message comes from a moderated member The message has a matching Approve or Approved header. The message has no valid senders The message is larger than the {} KB maximum size The message is not from a list member The message's content type was explicitly disallowed The message's content type was not explicitly allowed The message's file extension was explicitly disallowed The message's file extension was not explicitly allowed The results of your email command are provided below.
 The results of your email commands The sender is in the nonmember {} list The variable 'm' is the ${listspec} mailing list The virgin queue pipeline. The {} list has {} moderation requests waiting. There are two ways to use this script: interactively or programmatically.
Using it interactively allows you to play with, examine and modify a mailing
list from Python's interactive interpreter.  When running interactively, the
variable 'm' will be available in the global namespace.  It will reference the
mailing list object. This script provides you with a general framework for interacting with a
mailing list. Today's Topics (${count} messages) Today's Topics:
 Undefined domain: ${domain} Undefined runner name: ${name} Unknown list style name: ${style_name} Unrecognized or invalid argument(s):
{} Unshunt messages. Unsubscription request User: {}
 Welcome to the "${mlist.display_name}" mailing list${digmode} You are not allowed to post to this mailing list From: a domain which publishes a DMARC policy of reject or quarantine, and your message has been automatically rejected.  If you think that your messages are being rejected in error, contact the mailing list owner at ${listowner}. You are not authorized to see the membership list. You can print the list's posting address by running the following from the
command line:

% mailman withlist -r listaddr -l ant@example.com
Importing listaddr ...
Running listaddr.listaddr() ...
ant@example.com You have been invited to join the ${event.mlist.fqdn_listname} mailing list. You have been unsubscribed from the ${mlist.display_name} mailing list You will be asked to confirm your subscription request and you may be issued a
provisional password.

By using the 'digest' option, you can specify whether you want digest delivery
or not.  If not specified, the mailing list's default delivery mode will be
used.  You can use the 'address' option to request subscription of an address
other than the sender of the command.
 Your confirmation is needed to join the ${event.mlist.fqdn_listname} mailing list. Your confirmation is needed to leave the ${event.mlist.fqdn_listname} mailing list. Your message to ${mlist.fqdn_listname} awaits moderator approval Your new mailing list: ${fqdn_listname} Your subscription for ${mlist.display_name} mailing list has been disabled Your urgent message to the ${mlist.display_name} mailing list was not
authorized for delivery.  The original message as received by Mailman is
attached.
 [${mlist.display_name}]  [----- end pickle -----] [----- start pickle -----] [ADD] %s [DEL] %s [No bounce details are available] [No details are available] [No reason given] [No reasons given] bad argument: ${argument} domain:admin:notice:new-list.txt help.txt ipython is not available, set use_ipython to no list:admin:action:post.txt list:admin:action:subscribe.txt list:admin:action:unsubscribe.txt list:admin:notice:disable.txt list:admin:notice:increment.txt list:admin:notice:pending.txt list:admin:notice:removal.txt list:admin:notice:subscribe.txt list:admin:notice:unrecognized.txt list:admin:notice:unsubscribe.txt list:member:digest:masthead.txt list:member:generic:footer.txt list:user:action:invite.txt list:user:action:subscribe.txt list:user:action:unsubscribe.txt list:user:notice:hold.txt list:user:notice:no-more-today.txt list:user:notice:post.txt list:user:notice:probe.txt list:user:notice:refuse.txt list:user:notice:rejected.txt list:user:notice:warning.txt list:user:notice:welcome.txt n/a not available readline not available slice and range must be integers: ${value} {} Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-24 21:56+0000
Last-Translator: Tomasz Berner <tom@lodz.pl>
Language-Team: Polish <https://hosted.weblate.org/projects/gnu-mailman/mailman/pl/>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Weblate 5.10.2-dev
 
    Uruchamia skrypt. Argumentem jest ścieżka modułu do wywoływanego skryptu. 
    Ta zostanie zaimportowana, a następnie, jeśli podano również --listspec/-l, zostanie
    wywoływana z listą mailingową jako pierwszym argumentem.  Jeśli na końcu
     wiersza poleceń są podane dodatkowe argumenty, są one przekazywane jako
    kolejne argumenty pozycyjne do wywołania.  Aby uzyskać dodatkową pomoc, zobacz
    --details.

    Jeśli nie podano argumentu --listspec/-l, wywoływana funkcja skryptu
    nie ma argumentów.
     
    Uruchamia wymieniony runner, który musi być jednym z ciągów zwracanych
przez opcję -l.

    Dla runnerów zarządzających katalogiem kolejek, opcjonalne `slice:range`, jeśli zostało podane
    jest używana do przypisania wielu procesów runnera do tej kolejki.
    range jest całkowitą liczbą runnerów dla kolejki, podczas gdy slice jest numerem tego
    runnera z zakresu [0..range).  Dla runnerów, którzy nie zarządzają kolejką, slice i
    range są ignorowane.

    Kiedy używana jest forma `slice:range`, należy upewnić się, że każdy runner dla kolejki
    otrzymał tę samą wartość zakresu.  Jeśli `slice:runner` nie jest podane, to
    przyjmowane są wartości 1:1.
     
- Wykonano. 
- Zignorowane: 
- Wyniki: 
- Nieprzetworzone: 
Wstrzymane Wiadomości:
 
Wstrzymane Subskrypcje:
 
Wstrzymane Rezygnacje z Subskrypcji:
 
Zastąpiono część multipart/alternative pierwszą alternatywą.
 
___________________________________________
Filtrowanie zawartości Mailman'a usunęło
następujące części MIME z tej wiadomości.
     Bardziej szczegółowe dane wyjściowe, w tym ścieżki systemu plików,
    z których korzysta Mailman.     Określenie listy mailingowej, która ma być obsługiwana. Może to być adres
    wysyłkowy listy lub jej List-ID.  Argumentem może być również wyrażenie regularne Pythona,
    w którym to przypadku jest ono dopasowywane zarówno do adresu wysyłkowego
    i List-ID wszystkich list mailingowych. Aby użyć wyrażenia regularnego,
    LISTSPEC musi zaczynać się od ^ (a dopasowanie jest wykonywane za pomocą re.match().
    LISTSPEC nie może być wyrażeniem regularnym, chyba że podano opcję --run.     Dodaje wszystkie adresy członkowskie w FILENAME z trybem dostarczania określonym
    przez -d/--delivery.  FILENAME może zawierać '-', aby wskazać standardowe wejście.
    Puste linie i linie zaczynające się od '#' są ignorowane.
         Dodawaj i usuwaj użytkownikóœ w razie potrzeby, aby zsynchronizować członkostwo listy
    z plikiem wejściowym. FILENAME jest plikiem zawierającym nowe członkostwo,
    po jednym członku w każdym wierszu. Puste linie i linie zaczynające się od „#” są
    ignorowane. Adresy w FILENAME, które nie są aktualnymi subskrybentami listy
    zostaną dodane do listy z trybem dostarczania określonym przez
    -d/--delivery. Użytkownicy listy, których adresy nie znajdują się w FILENAME, zostaną
    usunięci z listy. FILENAME może zawierać '-', aby wskazać standardowe wejście.
         Dodawanie i/lub usuwanie właścicieli i moderatorów listy.
         Dodatkowe pary klucz/wartość metadanych do dodania do słownika metadanych wiadomości
    Należy użyć formatu klucz=wartość.
    Dozwolonych jest wiele opcji -m.     Zmiana adresu e-mail użytkownika z old_address na z ewentualnie zachowaną wielkością liter
    new_address.
         Plik konfiguracyjny do użycia, Jeśli nie zostanie podany, zmienna środowiskowa
    MAILMAN_CONFIG_FILE jest sprawdzana i używana, jeśli jest ustawiona.
    Jeśli nie podano żadnej z nich, ładowany jest domyślny plik konfiguracyjny.     Utwórz listę mailingową.

    Wymagana jest „w pełni kwalifikowana nazwa listy”, tj. adres pocztowy listy mailingowej
    jest wymagany.  Musi to być prawidłowy adres e-mail, a domena musi być
    zarejestrowana w Mailman'ie. Nazwy list są wymuszane małymi literami.     Data: ${date}     Usuwa wszystkich subskrybentów listy. Jeśli podano, żadna z opcji -f/--file,
    -m/--członek lub --fromall nie mogą być wyspecyfikowane.
         Usuwa subskrybentów listy, których adresy znajdują się w FILENAME, dodatkow tych
    określonymi przez -m/--member, jeśli ta opcja istnieje. FILENAME może być '-', aby wskazać
    standardowe wejście. Puste linie i linie zaczynające się od '#' są ignorowane.
         Usuwanie członków z listy mailingowej.     Usuwa subskrybentów listy, których adres to ADDRESS, dodatkowo usuwa adresy
    określone przez -f/--file, jeśli taka istnieje. Opcja ta może być powtarzana dla
    wielu adresów.
         Usuwa subskrybentów określonych przez -m/--member i/lub -f/--file ze wszystkich list w instalacji.
    Nie może to być użyte razem z parametrami
    -a/--all lub -l/--list.
         Odrzuca wszystkie pominięte wiadomości zamiast przenosić je z powrotem
    do ich oryginalnej kolejki.     Wyświetla członków listy mailingowej.
    Podczas wyświetlania można filtrować według różnych kryteriów.
    Bez podanych opcji, wyświetlanie członków listy mailingowej
    na standardowe wyjście jest trybem domyślnym.
         Wyświetla wszystkie subskrypcje dla użytkownika lub użytkowników z adresem pasującym do
    wzorca. Ponieważ część procesu obejmuje konwersję wzorca
    na zapytanie SQL z symbolami wieloznacznymi, wzorzec powinien być prosty. Prosty tekst
    działa najlepiej.
         Wyświetla tylko elementy typu paczka.
    'any' oznacza dowolny typ paczki, 'plaintext' oznacza tylko zwykły tekst (rfc 1153)
    'mime' oznacza paczki typu MIME.     Wyświetla tylko członków z daną ROLĄ.
    Rola może być 'any', 'member', 'nonmember', 'owner', 'moderator',
    lub 'administrator' (tj. właściciele i moderatorzy).
    Jeśli nie zostanie podana, przyjmowana jest rola 'member'.     Wyświetla tylko subsrybentów z danym statusem dostawy.
    „enabled” oznacza wszystkich członków, których dostarczanie jest włączone, »any« oznacza
    członków, których dostarczanie jest wyłączone z dowolnego powodu, „byuser” oznacza
    oznacza, że użytkownik wyłączył własne dostarczanie, „bybounces” oznacza, że
    dostarczanie zostało wyłączone przez automatyczny procesor odrzuceń,
    „byadmin” oznacza, że dostarczanie zostało wyłączone przez administratora lub moderatora listy.
    przez administratora lub moderatora listy, a „unknown” oznacza, że dostarczanie zostało wyłączone
    z nieznanych (starszych) powodów.     Wyświetla tylko członkostwa z podaną rolą. Jeśli nie podano,
    domyślnie wyświetlana jest rola 'all', czyli wszystkie role.     Wyświetlaj tylko członków z dostawą regularną.     Kieruje dane wyjściowe do FILENAME zamiast stdout. FILENAME
    może być '-', aby wskazać standardowe wyjście.     W rzeczywistości nic nie robi, ale w połączeniu z --verbose, pokazuje co
    by się stało.     W rzeczywistości nie wprowadzaj zmian. Zamiast tego wyświetl, co zostanie
    zmienione na liście.     Nie próbuj ładnie wyświetlać obiektu. Jest to przydatne, jeśli istnieje jakiś
    problem z obiektem i po prostu chcesz uzyskać niepiklowaną jego
    reprezentację. Przydatne z „mailman qfile -i <plik>”. W takim przypadku
    lista niepiklowana obiektów zostanie pozostawiona w zmiennej o nazwie 'm'.     Nie wyświetlaj komunikatów o stanie. Komunikaty o błędach są nadal wysyłłane do standardowego
    potoku error.     Nie uruchamiaj ponownie runner'ów , gdy zakończą działanie z powodu błędu lub SIGUSR1.
    Używaj tego tylko do debugowania.     Adres e-mail użytkownika, który ma zostać usunięty z danej roli.
    Można powtórzyć, aby usunąć wielu użytkowników.
         Plik do wysłania danych wyjściowych. Jeśli nie podano lub podano „-”, używane jest standardowe
    wyjście.     Plik, do którego mają zostać wysłane dane wyjściowe. Jeśli nie zostanie podany, używane jest standardowe wyjście.     Od: ${from_}     Generowanie plików aliasów MTA podczas uruchamiania. Niektóre MTA, takie jak postfix, nie mogą
    dostarczać wiadomości e-mail, jeśli pliki aliasów wymienione w jego konfiguracji nie są
    obecne. W niektórych sytuacjach może to prowadzić do impasu przy pierwszym
    uruchomieniu serwera mailman3. Ustawienie tej opcji na true sprawi, że skrypt
    utworzy pliki, a tym samym pozwoli MTA działać płynnie.     Jeśli obserwator główny znajdzie istniejącą blokadę główną, zwykle zakończy działanie
    z komunikatem o błędzie. Z tą opcją,  wykona dodatkowy etap sprawdzania.
    Jeśli proces pasujący do hosta/pid opisanego w pliku
    blokady jest uruchomiony,  nadal wyjdzie, wymagając ręcznego wyczyszczenia blokady. 
    Ale jeśli nie zostanie znaleziony żaden pasujący proces, 
    usunie wyglądającą na nieaktualną blokadę i podejmie kolejną próbę przejęcia
    blokady głównej.     Jeśli obserwator główny znajdzie istniejącą blokadę główną, zwykle zakończy działanie
    z komunikatem o błędzie. Z tą opcją,  wykona dodatkowy etap sprawdzania.
    Jeśli proces pasujący do hosta/pid opisanego w pliku blokady
    jest uruchomiony, nadal wyjdzie, wymagając ręcznego wyczyszczenia blokady.
    Ale jeśli nie zostanie znaleziony żaden pasujący proces, usunie wyglądającą
    na nieaktualną blokadę i podejmie kolejną próbę przejęcia blokady
    głównej.     Jeśli jesteś pewien, że chcesz uruchomić program jako root, podaj --run-as-root.     Import danych listy z Mailman 2.1. Wymaga podania w pełni kwalifikowanej nazwy listy
    do zaimportowania i ścieżka do pliku pickle programu Mailman 2.1.     Zwiększenie numeru woluminu paczki i zresetowanie numeru paczki do jeden. Jeśli
    użyte z --send, numer woluminu jest zwiększany przed wysłaniem bieżących
    paczek.     Klucz używany do wyszukiwania. Jeśli nie podano sekcji, wyświetlone zostaną wszystkie pary klucz-wartość
    z dowolnej sekcji pasującej do podanego klucza.     Pozostawia użytkownika w interaktywnym wierszu poleceń po zakończeniu wszystkich innych procesów przetwarzania.
    Jest to ustawienie domyślne, chyba że podano opcję --run.     Wyświetla listę tylko tych list mailingowych, które są hostowane w podanej domenie, która
    musi być nazwą hosta poczty e-mail. Można podać wiele opcji -d.
         Master obserwator podprocesów.

    Uruchamia i obserwuje skonfigurowanych runnerów, upewniając się, że pozostaną przy życiu.
    żywe.  Każdy runner jest kolejno rozwidlany i wykonywany, a master oczekuje
    na ich identyfikatory procesów. Gdy wykryje, że runner podrzędny zakończył działanie, może go
    uruchomić ponownie.

    Runnery reagują na SIGINT, SIGTERM, SIGUSR1 i SIGHUP.  SIGINT,
    SIGTERM i SIGUSR1 powodują, że runner kończy działanie.  Master będzie
    uruchomiał ponownie runnery, które zakończyły działanie z powodu SIGUSR1 lub innego powodu
    (powiedzmy z powodu nieprzechwyconego wyjątku).  SIGHUP powoduje, że
    master i runnery zamykają swoje pliki dziennika i otwierają je ponownie po
    następnej wydrukowanej wiadomości.

    Master odpowiada również na SIGINT, SIGTERM, SIGUSR1 i SIGHUP, które po prostu
    przekazuje runnerom.  Należy zauważyć, że master zamknie i
    ponownie otworzy własne pliki dziennika po otrzymaniu SIGHUP.  Master pozostawia również
    swój własny identyfikator procesu w pliku określonym w pliku konfiguracyjnym, ale zwykle
    nie trzeba używać tego PID bezpośrednio.     Message-ID: ${message_id}     Nazwa pliku zawierającego wiadomość do wstrzyknięcia. Jeśli nie podano lub
    '-' (bez cudzysłowów) używane jest standardowe wejście.     Zwykle skrypt ten odmówi uruchomienia, jeśli identyfikator użytkownika i identyfikator grupy nie są
    ustawione na użytkownika i grupę „mailman” (jak zdefiniowano podczas konfigurowania
    Mailman).  Jeśli zostanie uruchomiony jako root, skrypt zmieni się na tego użytkownika i grupę
    przed wykonaniem sprawdzenia.

    Może to być niewygodne dla celów testowania i debugowania, więc flaga -u
    oznacza, że krok, który ustawia i sprawdza uid/gid jest pomijany, a program jest uruchamiany
    z bieżący użytkownikiem i grupą. Ta flaga nie jest zalecana
    dla normalnych środowisk produkcyjnych.

    Zauważ jednak, że jeśli uruchamiasz z -u i nie jesteś w grupie mailman, 
    mogą wystąpić problemy z uprawnieniami, takie jak niemożność usunięcia archiwów listy
    z wykorzystaniem interfejsu web. Powodzenia!     Powiadomienie właściciela listy e-mailem, że jego lista mailingowa została
    utworzona.     Działa na liście mailingowej.

    Aby uzyskać szczegółową pomoc, zobacz --details
         Działa na tej liście mailingowej. Można podać wiele opcji --list. Argument
    może być List-ID lub w pełni kwalifikowaną nazwą listy. Bez
    tej opcji, operuje na paczkach dla wszystkich list mailingowych.     Działa na tej liście mailingowej. Można podać wiele opcji --list. Argument
    może być identyfikatorem listy lub w pełni kwalifikowaną nazwą listy. Bez
    tej opcji, operuje na prośbach dla wszystkich list mailingowych.     Zastępuje domyślny zestaw runnerów, które będą wywoływane przez mastera.
    zazwyczaj zdefiniowanych w pliku konfiguracyjnym. Można podać wiele opcji -r.
    Wartości opcji -r są przekazywane bezpośrednio do bin/runner.     Zastępuje ustawienie listy dla admin_notify_mchanges.     Zastępuje ustawienie listy dla send_goodbye_message dla
    usuniętych członków.     Zastępuje ustawienie listy dla send_welcome_message dla dodanych członków.     Zastępuje ustawienie listy dla send_welcome_message.     Zarejestruj domenę listy mailingowej, jeśli nie została jeszcze zarejestrowana.
    Jest to domyślne zachowanie, ale te opcje są dostępne dla wstecznej
    kompatybilności. Z opcją -D nie rejestruje domeny listy mailingowej.     Uruchom wskazany runner dokładnie raz w trakcie jego głównej pętli. W przeciwnym razie
    runner działa w nieskończoność, dopóki nie otrzyma sygnału zatrzymania. Nie jest to
    kompatybilne z runner'ami, których nie można uruchomić raz.     Uruchamianie poleceń mailman'a jako root nie jest zalecane i mailman
    odmówi uruchomienia jako root, chyba że ta opcja zostanie określona.     Sekcja do użycia w wyszukiwaniu.  Jeśli nie podano klucza, wyświetlone zostaną wszystkie pary klucz-wartość
    danej sekcji.     Wysyła wszystkie zebrane paczki dla listy tylko wtedy, gdy ich digest_send_periodic
    jest ustawiona na True.     Wyślij teraz wszystkie zebrane paczki, nawet jeśli próg rozmiaru
    nie został jeszcze osiągnięty.     Wyślij dodawanym użytkownikom zaproszenie, zamiast od razu ich dodawać.
         Ustaw tryb dostarczania dodanych subskrybentów na „regular”, „mime”, „plain”,
    „summary” lub »disabled«. Tj. jeden z regularnych, trzy tryby paczki
    lub brak dostarczania. Jeśli nie podano nic, domyślnym trybem jest "regular".     Ustaw tryb dostarczania dodanych subskrybentów na „regular”, „mime”, „plain”,
    „summary” lub »disabled«.  Tj. jeden z regularnych, trzy tryby paczek
    lub brak dostarczania. Jeśli nie podano, domyślnym trybem jest "regular". Ignorowane dla zaproszonych
    użytkowników.     Ustaw preferowany język listy na CODE, który musi być zarejestrowanym
    dwuliterowym kodem języka.     Określ adres e-mail właściciela listy. Jeśli adres nie jest obecnie
    zarejestrowany w Mailman'ie, zostanie zarejestrowany i powiązany z użytkownikiem.
    Mailman wyśle na ten adres wiadomość z potwierdzeniem,
    a także powiadomienie o utworzeniu listy.
    Można określić więcej niż jednego właściciela listy.     Określ nazwę stylu listy.     Określa kodowanie łańcuchów w PICKLE_FILE, jeśli nie jest to utf-8 lub jego podzbiór.
    Zazwyczaj będzie to zestaw znaków Mailman 2.1 list określony przez
    preferred_language.     Uruchomienie runnera.

    Runner wskazany w wierszu poleceń jest uruchamiany allbo
    przez swoją główną pętlę raz (dla tych runnerów, które to obsługują) albo
    w sposób ciągły.  W tym drugim przypadku główny runner uruchamia
    wszystkie swoje podprocesy.

    -r jest wymagane, chyba że podano -l lub -h, a jego argumentem musi być jedna z
    nazw wyświetlanych przez przełącznik -l.

    Normalnie, skrypt ten powinien być uruchamiany z `mailman start`.  Uruchomienie go
    osobno lub z -o jest użyteczne tylko do debugowania. Po uruchomieniu
    w ten sposób, zmienna środowiskowa $MAILMAN_UNDER_MASTER_CONTROL będzie
    ustawiona zmieniając niektóre zachowania związane z obsługą błędów.
         Uruchom interaktywną sesję Pythona ze zmienną o nazwie 'm'
    zawierającą listę niepiklowanych obiektów.     Temat: ${subject}     Polecenie gatenews jest uruchamiane okresowo przez runner nntp.
    Jeśli uruchamiasz je za pomocą crona, powinieneś usunąć je z crontaba.
    Jeśli chcesz uruchomić je ręcznie, ustaw _MAILMAN_GATENEWS_NNTP
    w  środowisku.     Lista, na której mają być wykonywane operacje. Wymagane, chyba że podano --fromall.
         Nazwa kolejki, do której ma zostać wstrzyknięta wiadomość.
    QUEUE musi być jednym z katalogów wewnątrz katalogu kolejki.
    W przypadku pominięcia, używana jest kolejka przychodząca.     Rola do dodania/usunięcia. Może to być „owner” lub „moderator”.
    Jeśli nie zostanie podana, zakładana jest rola „owner”.
         Ta opcja nie ma żadnego wpływu. Istnieje tylko dla kompatybilności wstecznej.     Użytkownik do dodania z daną rolą. Może to być adres e-mail lub, jeśli
    podany, dowolna wyświetlana nazwa i adres e-mail parsowalne przez
    email.utils.parseaddr. Np. „Jane Doe <jane@example.com>”. Może być powtarzane
    aby dodać wielu użytkowników.
         [MODE] Dodaj wszystkie adresy subskrybentów w FILENAME. Ta opcja została usunięta.
    Zamiast niej należy użyć „mailman addmembers”.     [MODE] Usuń wszystkie adresy subskrybentów znalezione w FILENAME.
    Ta opcja została usunięta. Zamiast niej należy użyć „mailman delmembers”.     [MODE] Synchronizuje wszystkie adresy subskrybentów określonej listy mailingowej
    z adresami znalezionymi w FILENAME.
    Ta opcja została usunięta. Zamiast niej należy użyć „mailman syncmembers”.  (Tryb paczek) Znaleziono ${count} pasujących list mailingowych: ${display_name} potwierdzenie postu ${email} jest już w roli ${role.name} dla listy ${mlist.fqdn_listname} ${email} nie jest w roli ${role.name} dla listy ${mlist.fqdn_listname} ${member} został wypisany z listy ${mlist.display_name} z powodu zbyt wielu zwrotów Punktacja za zwroty ${member} dla listy ${mlist.display_name} została zwiększona Subskrypcja listy ${mlist.display_name} użytkownika ${member} została wyłączona ${mlist.display_name} Paczka, Wolumen ${volume}, Wydanie ${digest_number} Wiadomość sonda listy {mlist.display_name} Powiadomienie o subskrypcji ${mlist.display_name} ${mlist.display_name} niewyłapane powiadomienie o zwrocie ${mlist.display_name} powiadomienie o rezygnacji z subskrypcji ${mlist.fqdn_listname} post od ${msg.sender} wymaga zatwierdzenia ${mlist.list_id} przeniesiony do wolumenu ${mlist.volume}, numer ${mlist.next_digest_number} ${mlist.list_id} nie ma subskrybentów ${mlist.list_id} znajduje się w wolumenie ${mlist.volume}, numer ${mlist.next_digest_number} ${mlist.list_id} wysłany wolumen ${mlist.volume}, numer ${mlist.next_digest_number} ${name} uruchamia ${classname} ${person} ma oczekującą subskrypcję dla ${listname} ${person} opuścił ${mlist.fqdn_listname} ${realname} przez ${mlist.display_name} ${self.email} jest już członkiem listy mailingowej ${self.fqdn_listname} ${self.email} jest już moderatorem listy mailingowej ${self.fqdn_listname} ${self.email} jest już nie-subsrybentem listy mailingowej ${self.fqdn_listname} ${self.email} jest już właścicielem listy mailingowej ${self.fqdn_listname} ${self.name}: ${email} nie jest subskrybentem ${mlist.fqdn_listname} ${self.name}: Nie znaleziono prawidłowego adresu do subskrypcji ${self.name}: Nie znaleziono prawidłowego adresu e-mail do anulowania subskrypcji ${self.name}: brak takiego polecenia: ${command_name} ${self.name}: zbyt wiele argumentów: ${printable_arguments} (brak tematu) - Szczegóły oryginalnej wiadomości: -------------- next part --------------
 Flagi --send i --periodic nie mogą być używane razem <----- początek obiektu ${count} -----> Usunięto część wiadomości niekompatybilną z paczką pain/text ...
Nazwa: ${filename}
Typ: ${ctype}
Rozmiar: ${size} bajtów
Opis: ${desc}
 Poprzednie uruchomienie GNU Mailman nie zakończyło się poprawnie ({}). Spróbuj użyć --force Reguła, która zawsze pasuje. Akceptuj wiadomość. Dodaje prefiks specyficzny dla listy do wartości nagłówka Subject. Dodaj nagłówki RFC 2369 List-*. Dodaj wiadomość do archiwum. Dodaje wiadomość do paczki, ewentualnie wysyła ją. Adres został zmieniony z {} na {}. Adres {} już istnieje; nie można go zmienić. Adres {} nie jest prawidłowym adresem e-mail. Nie znaleziono adresu {}. Adresy nie różnią się. Nic nie trzeba zmieniać. Po przefiltrowaniu treści wiadomość była pusta Już subskrybowane (pomijanie): ${email} Alias dla „end”. Alias dla 'join'. Alias dla „leave”. Alternatywny katalog, do którego wysyłane są różne pliki MTA. Adres zgłaszania próśb listy można wyświetlić, uruchamiając następujące polecenie:

% mailman withlist -r listaddr.requestaddr -l ant@example.com
Importowanie listaddr ...
Uruchamianie listaddr.requestaddr() ...
ant-request@example.com Ogłoszeniowy styl listy mailingowej. Zastosuj ograniczanie DMARC. Jako inny przykład, powiedzmy, że chcesz zmienić wyświetlaną nazwę dla konkretnej listy mailingowej.
Mógłbyś umieścić następującą funkcję w pliku o nazwie
`change.py`:

def change(mlist, display_name):
    mlist.display_name = display_name

i uruchomić ją z wiersza poleceń:

% mailman withlist -r change -l ant@example.com 'My List'

Zwróć uwagę, że nie musisz jawnie zatwierdzać żadnych transakcji w bazie danych, ponieważ
Mailman zrobi to za Ciebie (zakładając, że nie wystąpiły żadne błędy). Automatyczna odpowiedź na wiadomość do listy mailingowej "${display_name}" Zła specyfikacja runner'a: ${value} Określ właścicieli i moderatorów. Wskazywanie regularnych odbiorców wiadomości. Nie można zaimportować modułu runner'a: ${class_path} Nie można przetworzyć jako prawidłowy adres e-mail (pomijanie): ${line} Nie można przywrócić pominiętej wiadomości ${filebase}, pomijając:
${error} Wykrywa wiadomości, które są większe niż określone maksimum. Wykryj wiadomości ze Tematem paczki lub standardowym cytatem. Przechwyć wiadomości z jawnym adresem docelowym. Wykryj wiadomości bez nagłówka lub z pustym nagłówkiem tematu. Przechwyć wiadomości z podejrzanymi nagłówkami. Wykrywanie wiadomości ze zbyt dużą liczbą jawnych odbiorców. Wyłapywanie błędnie zaadresowanych poleceń e-mail. Wyczyść określone nagłówki ze wszystkich wiadomości. Potwierdzenie prośby subskrypcji lub wstrzymanej wiadomości. Wiadomość e-mail z potwierdzeniem wysłana do ${person} E-mail z potwierdzeniem wysłany do ${person} w celu opuszczenia listy ${mlist.fqdn_listname} Token potwierdzenia nie pasuje Potwierdzono Powiadomienie o wiadomości z filtrem treści Utworzono listę mailingową: ${mlist.fqdn_listname} Moderacja DMARC Udekoruj wiadomość nagłówkami i stopkami. Stopka Paczki Nagłówek Paczki Odrzuca wiadomości i zatrzymuje przetwarzania. Styl listy dyskusyjnej z prywatnym archiwum. Wyświetl wersję Mailman'a. Wyświetla więcej informacji o debugowaniu w pliku dziennika. Zwraca polecenie i jego argumenty. Email: {} Dla tej listy obowiązuje moderacja awaryjna Koniec  Filtrowanie zawartości MIME wiadomości. Znajdź politykę DMARC dla domeny z nagłówka From:. Z nieznanych przyczyn nie udało się uzyskać blokady głównej.

Plik blokady: ${config.LOCK_FILE}
Host blokady: ${hostname}

Wyjście. Przekazanie moderowanej wiadomości GNU Mailman jest już uruchomiony GNU Mailman jest w nieoczekiwanym stanie (${hostname} != ${fqdn_name}) GNU Mailman nie jest uruchomiony GNU Mailman jest uruchomiony (master pid: ${pid}) GNU Mailman został zatrzymany (nieaktualny pid: ${pid}) Generowanie map aliasów MTA Pobiera listę subskrybentów listy. Uzyskaj pomoc dotyczącą dostępnych poleceń e-mail. Pobieranie informacji z pliku kolejki. Pobiera odbiorców regularnych z dołączonego pliku. Nagłówek „{}” pasował do linii bounce_matching_header Nagłówek "{}” pasował do reguły nagłówka Oto przykład użycia opcji --run.  Powiedzmy, że masz plik w katalogu instalacyjnym
Mailman o nazwie „listaddr.py”, z następującymi dwiema 
funkcjami:

def listaddr(mlist):
    print(mlist.posting_address)

def requestaddr(mlist):
    print(mlist.request_address)

Metody Run przyjmują co najmniej jeden argument, obiekt listy mailingowej, na którym mają działać.
Wszelkie dodatkowe argumenty podane w wierszu poleceń są przekazywane 
jako argumenty pozycyjne do wywoływanej metody.

Jeśli nie podano -l, można uruchomić funkcję, która nie przyjmuje żadnych argumentów.
 Wstrzymanie wiadomości i zatrzymanie przetwarzania. Jeśli odpowiesz na tę wiadomość, zachowując nienaruszony nagłówek Tematu:, Mailman
zignoruje zatrzymaną wiadomość. Zrób to, jeśli wiadomość jest spamem. Jeśli odpowiesz
i dołączysz nagłówek Approved: z hasłem listy, wiadomość zostanie zatwierdzona
do wysłania na listę.  Nagłówek Approved: może również pojawić się
w pierwszym wierszu treści odpowiedzi. Ignorowanie niesłownikowych: {0!r} Ignorowanie non-text/plain części MIME Nieprawidłowa nazwa listy: ${fqdn_listname} Nieprawidłowy adres właściciela: ${invalid} Informacje o tej instancji Mailman'a. Wstrzykuje wiadomość z pliku do kolejki listy mailingowej. Nieprawidłowe Zatwierdzenie: hasło Nieprawidłowy adres e-mail: ${email} Nieprawidłowy kod języka: ${language_code} Nieprawidłowy lub niezweryfikowany adres e-mail: ${email} Nieprawidłowa wartość dla [shell]use_python: {} Czy master w ogóle działa? Dołącz do tej listy mailingowej. Ostatnie powiadomienie o automatycznej odpowiedzi na dziś Opuść tę listę mailingową. Opuść tę listę mailingową.

Możesz zostać poproszony o potwierdzenie swojej prośby. Mniej gadatliwości Wymień wszystkie listy mailingowe. Lista już istnieje: ${fqdn_listname} Wymieniaj tylko te listy mailingowe, które są publicznie reklamowane Wyświetl dostępnych runner'ów i wyjdź. Lista: {} Poszukaj pętli wysyłania. Poszukaj wcześniejszych trafień reguły. Dopasowuje wszystkie wiadomości wysłane do listy mailingowej, 
        która będą kierowane do moderowanej grupy dyskusyjne Usenetj.
         Dopasowywanie wiadomości wysyłanych przez zablokowane adresy. Dopasowywanie wiadomości wysyłanych przez moderowanych użytkowników. Dopasuj wiadomości wysłane przez osoby niebędące subskrybentami. Dopasowanie wiadomości bez prawidłowych nadawców. Użytkownik nie subskrybuje (pomija): ${email} Subskrybenci listy mailingowej {}:
{} Subskrypcja jest zablokowane (pomijanie): ${email} Wiadomość ${message} Wiadomość zawiera informacje administracyjne Wiadomość ma temat paczki Wiadomość została już opublikowana na tej liście Wiadomość ma niejawne miejsce docelowe Wiadomość ma więcej niż {} odbiorców Wiadomość nie ma tematu Wiadomość cytuje standardową paczkę Nadawca wiadomości {} jest zablokowany dla tej listy Brak danych dla prośby {}

 Łańcuch moderacji Zmodyfikuj nagłówki wiadomości. Przeniesienie wiadomości do kolejki wychodzących wiadomości Usenet. ND. Nazwa: ${fname}
 Nowa prośba o subskrypcję ${self.mlist.display_name} od ${self.address.email} Nowa prośba o rezygnację z subskrypcji listy ${mlist.display_name} od ${email} Nowa prośba o rezygnację z subskrypcji ${self.mlist.display_name} od ${self.address.email} Brak dziecka o identyfikatorze pid: ${pid} Nie znaleziono tokena potwierdzającego Nie znaleziono pasujących list mailingowych Brak zarejestrowanego użytkownika dla adresu e-mail: ${email} Nie podano nazwy runner'a. W wiadomości nie znaleziono nadawcy. Brak takiego polecenia: ${command_name} Nie znaleziono takiej listy: ${spec} Brak listy pasującej do specyfikacji: ${listspec} Brak takiej listy: ${_list} Brak takiej listy: ${listspec} Brak takiej kolejki: ${queue} Nie jest to plik konfiguracyjny Mailman'a 2.1: ${pickle_file} Nic do dodania ani usunięcia. Nic do zrobienia Powiadamianie właścicieli/moderatorów list o oczekujących prośbach. Liczba znalezionych obiektów (patrz zmienna 'm'): ${count} Działanie na paczkach. Styl standardowy listy dyskusyjnej. Wiadomość oryginalna PID nie do odczytania w: ${config.PID_FILE} Przeprowadzanie kontroli autoryzacji ARC i dołączanie nagłówków wynikowych Przeprowadzenie kontroli uwierzytelniania i dołączenie nagłówka Authentication-Results. Dokonaj księgowania po udanym poście. Odpytywanie serwera NNTP o wiadomości, które mają być przekazywane do list mailingowych. Publikowanie w moderowanej grup dyskusyjnych Usenet Publikowanie wiadomości zatytułowanej „${subject}” Wyświetl szczegółowe instrukcje i zakończ. Wyświetl mniej danych wyjściowych. Wyświetl dodatkowe informacje o stanie. Wyświetl konfigurację programu Mailman. Przetwarzanie ograniczenia odrzucenie lub zignorowania DMARC Tworzy listę nazw i adresów e-mail subskrybentów.

Opcjonalne argumenty delivery= i mode= mogą być użyte do ograniczenia raportu
do subskrybentów z pasującym statusem dostawy i/lub trybem dostawy. Jeśli
delivery= lub mode= jest określony więcej niż raz, używane jest tylko ostatnie
jego wystąpienie.
 Programowo można napisać funkcję do obsługi listy mailingowej, a skrypt
ten skrypt zajmie się utrzymaniem porządku (przykłady znajdują się poniżej).
W takim przypadku ogólna składnia użycia to:

% mailman withlist [options] -l listspec [args ...]

gdzie `listspec` to albo adres pocztowy listy mailingowej
(np. ant@example.com) lub identyfikator listy List-ID (np. ant.example.com). Powód: {}

 Zregeneruj aliasy odpowiednie dla twojego MTA. Wyrażenie regularne wymaga --run Odrzucenie/zwrot wiadomości i zatrzymanie przetwarzania. Usuń nagłówki DomainKeys. Usuwa listę mailingową (nie usuwa archiwów). Usunięta lista: ${listspec} Ponowne otwarcie runner'ów Mailman'a Prośba do listy mailingowej „${display_name}” została odrzucona Ponowne uruchamianie runner'ów Mailman'a Wysyłanie potwierdzenia wysłania wiadomości. Wysyłaj odpowiedzi automatyczne. Wysłanie wiadomości do kolejki wychodzącej. Nadawca: {}
 Wyświetlenie listy wszystkich dostępnych nazw kolejek i zakończenie. Pokaż również opisy list Pokaż również nazwy list Wyświetla bieżący status działania systemu Mailman. Wyświetl ten komunikat pomocy i wyjdź. Wyłączanie master runner'a Mailman'a Zasygnalizuj procesom Mailman'a, aby ponownie otworzyły swoje pliki dziennika. Usunięto nieaktualny plik pid. Uruchom procesy Mailman master i runner'y. Uruchamianie master runner'a Mailman'a Zatrzymanie i ponowne uruchomienie runner'ów Mailman'a. Zatrzymaj przetwarzanie poleceń. Zatrzymanie procesów Mailman master i runner'y. Temat: {}
 Subskrypcja już oczekuje na zatwierdzenie (pominięto): ${email} Prośba subskrypcji Eliminuje niektóre duplikaty tej samej wiadomości. Wyłączenie komunikatów o stanie Oznacz wiadomości etykietą tematu. Mailman Replybot Załączona wiadomość była zgodna z regułami filtrowania zawartości
listy mailingowej ${mlist.display_name} i nie została przekazana dalej
do subskrybentów listy. Otrzymujesz jedyną pozostałą kopię
odrzuconej wiadomości.

 Wbudowany łańcuch wysyłania właściciela. Wbudowany łańcuch dopasowywania nagłówka Wbudowany łańcuch moderacji. Wbudowany potok właścicielski. Wbudowany potok publikowania. Treść tej wiadomości została utracona. Prawdopodobnie została wysłana
na kilka list i została już obsłużona
 Lista mailingowa jest w trybie awaryjnej moderacji, a ta wiadomość nie została
        wstępnie zatwierdzona przez administratora listy.
         Nie można uzyskać blokady głównej, ponieważ wydaje się, 
że inna blokada główna jest już uruchomiona. Nie można uzyskać blokady głównej, ponieważ wygląda na to, że jakiś proces
na innym hoście mógł ją przejąć. Nie możemy przetestować  blokad poza
obrębem hosta, więc będziesz musiał wyczyścić ją ręcznie.

Plik blokady: ${config.LOCK_FILE}
Host blokady: ${hostname}

Wyjście. Nie udało się uzyskać blokady głównej. Wygląda na to, że istnieje nieaktualna blokada główna. 
Spróbuj ponownie uruchomić ${program} z flagą --force. Wiadomość pochodzi od moderowanego użytkownika Wiadomość ma pasujący nagłówek Approve lub Approved. Wiadomość nie ma prawidłowych nadawców Wiadomość jest większa niż maksymalny rozmiar {} KB Wiadomość nie pochodzi od subskrybenta listy Typ zawartości wiadomości był jawnie niedozwolony Typ zawartości wiadomości nie był jawnie dozwolony Rozszerzenie pliku wiadomości było jawnie niedozwolone Rozszerzenie pliku wiadomości nie było jawnie dozwolone Wyniki polecenia e-mail znajdują się poniżej.
 Wyniki Twoich poleceń e-mail Nadawca znajduje się na liście osób niebędących subskrybentami {} Zmienna 'm' to lista mailingowa ${listspec} Potok dziewiczej kolejki. Lista {} ma {} oczekujących próśb o moderacje. Istnieją dwa sposoby korzystania z tego skryptu: interaktywnie lub programowo.
Używanie go interaktywnie pozwala badać i modyfikować listę mailingową
z poziomu interaktywnego interpretera Pythona. Podczas uruchamiania interaktywnego,
zmienna „m” będzie dostępna w globalnej przestrzeni nazw. Będzie ona odwoływać się
do obiektu listy mailingowej. Ten skrypt zapewnia ogólny framework do interakcji 
z listą mailingową. Dzisiejsze Tematy (${count} wiadomości) Dzisiejsze Tematy:
 Niezdefiniowana domena: ${domain} Niezdefiniowana nazwa runner'a: ${name} Nieznana nazwa stylu listy: ${style_name} Nierozpoznane lub nieprawidłowe argumenty:
{} Przywrócenie pominiętych wiadomości. Prośba o rezygnację z subskrypcji Użytkownik: {}
 Witamy na liście mailingowej „${mlist.display_name}”${digmode} Nie możesz wysyłać wiadomości na tę listę mailingową z domeny, która publikuje politykę DMARC odrzucania lub kwarantanny, twoja wiadomość została automatycznie odrzucona. Jeśli uważasz, że Twoje wiadomości są błędnie odrzucane, skontaktuj się z właścicielem listy mailingowej pod adresem ${listowner}. Nie masz uprawnień do przeglądania listy subsrybentów. Adres pocztowy listy można wyświetlić, uruchamiając następujące polecenie z
wiersza poleceń:

% mailman withlist -r listaddr -l ant@example.com
Importowanie listaddr ...
Uruchamianie listaddr.listaddr() ...
ant@example.com Zostałeś zaproszony do dołączenia do listy ${event.mlist.fqdn_listname}. Zostałeś wypisany z listy mailingowej ${mlist.display_name} Zostaniesz poproszony o potwierdzenie prośby o subskrypcję i możesz otrzymać
hasło tymczasowe .

Korzystając z opcji „digest”, możesz określić, czy chcesz otrzymywać paczkę, czy nie.
Jeśli nie zostanie to określone, zostanie użyty domyślny tryb dostarczania dla listy mailingowej.
Możesz użyć opcji „address”, aby zażądać subskrypcji adresu
innego niż adres nadawcy polecenia.
 Potrzebne jest Twoje potwierdzenie dołączenia do listy ${event.mlist.fqdn_listname}. Potrzebne jest Twoje potwierdzenie opuszczenia listy ${event.mlist.fqdn_listname}. Twoja wiadomość do ${mlist.fqdn_listname} oczekuje na zatwierdzenie przez moderatora Twoja nowa lista mailingowa: ${fqdn_listname} Twoja subskrypcja listy ${mlist.display_name} została zawieszona Twoja pilna wiadomość do listy mailingowej ${mlist.display_name} nie została
autoryzowana do dostarczenia. Oryginalna wiadomość odebrana przez Mailman'a została
załączona.
 [${mlist.display_name}]  [----- koniec pickle -----] [----- początek pickle -----] [DODAJ] %s [USUŃ] %s [Szczegóły zwrotu nie są dostępne] [Brak szczegółowych informacji] [Nie podano przyczyny] [Nie podano powodów] zły argument: ${argument} Lista mailingowa „$listname” została właśnie utworzona.
Poniżej znajdują się podstawowe informacje o twojej liście mailingowej.

Istnieje interfejs e-mail dla użytkowników (nie administratorów) listy;
Informacje na temat korzystania z niego można uzyskać, wysyłając wiadomość ze słowem „help
jako temat lub treść wiadomości, na adres:

    $request_email

Wszelkie pytania należy kierować na adres $site_email. Pomoc dla listy mailingowej $listname

To jest polecenie e-mail 'help' dla wersji $version menedżera list GNU Mailman
pod adresem $domain.  Poniżej opisano polecenia, które można wysłać, aby uzyskać
informacje i kontrolować subskrypcję list Mailman w tej witrynie.
Polecenie może znajdować się w linii tematu lub w treści wiadomości.

Polecenia powinny być wysyłane na adres ${listname}-request@${domain}.

Opisy - słowa w „<>” oznaczają elementy WYMAGANE, a słowa w „[]”
oznaczają elementy OPCJONALNE.  Nie należy używać znaków „<>” lub
„[]” podczas używania poleceń.

Następujące polecenia są prawidłowe:

    $commands

Pytania i wątpliwości można kierować do:

    $administrator ipython nie jest dostępny, ustaw use_ipython na no Jako administrator listy jesteś proszony o autoryzację dla
następującej wiadomości skierowanej na listę mailingową:

    Lista:    $listname
    Od: $sender_email
    Temat: $subject

Wiadomość jest wstrzymana, ponieważ:

$reasons

W dogodnym dla siebie czasie odwiedź pulpit nawigacyjny, aby zatwierdzić lub odrzucić
żądanie. Wymagana jest twoja autoryzacja do zatwierdzenia prośby 
zapisania na listę mailingową:

    For:  $member
    Lista: $listname Wymagana jest twoja autoryzacja do zatwierdzenia prośby 
wypisania z listy mailingowej:

    For:  $member
    Lista: $listname Subskrypcja $member została wyłączona na liście $listname z powodu punktacji za zwroty
przekraczającej próg bounce_score_threshold listy mailingowej.

DSN powodujący niniejszą akcję, jeśli jest dostępny, jest dołączony do tej wiadomości. Punktacja zwrotów subskrybenta $member na $listname została zwiększona.

DSN powodujący niniejszą akcję, jeśli jest dostępny, jest dołączony do tej wiadomości. Lista $listname ma $count oczekujących zadań moderacji.

$data
Prosimy o zajęcie się tym jak najszybciej. Użytkownik $member został wypisany z listy $listname z powodu nadmiernej liczby zwrotów. Użytkownik $member został pomyślnie zasubskrybowany na listę $display_name. Załączona wiadomość została odebrana jako zwrot, ale albo format zwrotu
nie został rozpoznany albo nie można było wyodrębnić z niego żadnych adresów subskrybentów.
Ta lista mailingowa została skonfigurowana do wysyłania wszystkich nierozpoznanych wiadomości
do administratora(ów) listy. $member został usunięty z $display_name. Wysyłaj wiadomości na listę mailingową $display_name na adres
	$listname

Aby zapisać lub wypisać się z listy przez e-mail, wyślij wiadomość z tematem lub treścią
'help' na adres
	$request_email

Możesz skontaktować się z osobą zarządzającą listą pod adresem
	$owner_email

Odpowiadając, zmień temat wiadomości tak, aby był bardziej szczegółowy niż
„Re: Zawartość paczki $display_name...” _______________________________________________
Lista mailingowa $display_name -- $listname
Aby zrezygnować z subskrypcji, wyślij wiadomość e-mail na adres ${short_listname}-leave@${domain} Twój adres „$user_email” został zaproszony do dołączenia do listy mailingowej $short_listname
w domenie $domain przez właściciela listy mailingowej $short_listname.
Możesz zaakceptować zaproszenie po prostu odpowiadając na tę wiadomość.

Lub możesz dołączyć następującą linię - i tylko następującą linię
w wiadomości wysyłanej na adres $request_email:

    confirm $token

Zauważ, że zwykłe wysłanie `odpowiedzi' na tę wiadomość powinno działać z
większości czytników poczty.

Jeśli chcesz odrzucić to zaproszenie, po prostu zignoruj tę wiadomość.
Jeśli masz jakieś pytania, wyślij je na adres
$owner_email. Potwierdzenie rejestracji adresu e-mail

Witam, tu serwer GNU Mailman w domenie $.

Otrzymaliśmy prośbę o rejestrację adresu e-mail

    $user_email

Zanim będziesz mógł zacząć korzystać z GNU Mailman w tej witrynie, musisz najpierw potwierdzić, 
że jest to Twój adres e-mail. Możesz to zrobić odpowiadając na tę wiadomość.

Lub możesz dołączyć następującą linię - i tylko następującą linię
w wiadomości wysyłanej na adres $request_email:

    confirm $token

Zauważ, że zwykłe wysłanie `odpowiedzi' na tę wiadomość powinno działać z
większości czytników poczty.

Jeśli nie chcesz rejestrować tego adresu e-mail, po prostu zignoruj tę wiadomość.
Jeśli uważasz, że jesteś złośliwie subskrybowany na listę lub
masz inne pytania, możesz skontaktować się z

    $owner_email Potwierdzenie rezygnacji z subskrypcji adresu e-mail

Witam, tu serwer GNU Mailman w domenie $.

Otrzymaliśmy prośbę o anulowanie subskrypcji dla adresu e-mail

    $user_email

Zanim GNU Mailman będzie mógł anulować subskrypcję, musisz najpierw potwierdzić swoją prośbę.
Możesz to zrobić po prostu odpowiadając na tę wiadomość.

Lub możesz dołączyć następującą linię - i tylko następującą linię
w wiadomości wysyłanejna adres $request_email:

    confirm $token

Zauważ, że zwykłe wysłanie `odpowiedzi' na tę wiadomość powinno działać z
większości czytników poczty.

Jeśli nie chcesz zrezygnować z subskrypcji tego adresu e-mail, po prostu zignoruj tę wiadomość.
Jeśli uważasz, że zostałeś złośliwie wypisany z listy,
lub masz inne pytania, możesz skontaktować się z

    $owner_email Wiadomość do '$listname' z tematem

    $subject

Jest wstrzymana do czasu, aż moderator listy będzie mógł ją przejrzeć i zatwierdzić.

Wiadomość jest wstrzymana, ponieważ:

$reasons

Wiadomość zostanie opublikowana na liście lub otrzymasz 
powiadomienie o decyzji moderatora. Otrzymaliśmy wiadomość z adresu <$sender_email> z prośbą o
automatyczną odpowiedź z listy mailingowej $listname.

Liczba wiadomości, którą otrzymaliśmy dzisiaj z tego adresu to $count.
Aby uniknąć problemów takich jak pętle pocztowe między robotami pocztowymi,
nie będziemy dziś wysyłać żadnych dalszych odpowiedzi. Spróbuj ponownie jutro.

Jeśli uważasz, że ta wiadomość jest błędna lub jeśli masz jakieś pytania, prosimy
skontaktować się z właścicielem listy pod adresem $owner_email. Wiadomość zatytułowana

    $subject

została pomyślnie odebrana przez listę mailingową $display_name. To jest komunikat sondy pocztowej. Możesz zignorować tę wiadomość.

Lista mailingowa $listname otrzymała od ciebie pewną liczbę zwrotów,
co wskazuje, że może występować problem z dostarczaniem wiadomości na adres $sender_email.
Próbka jest załączona poniżej. Sprawdź tę wiadomość, aby upewnić się, że
nie ma problemów z adresem e-mail. Możesz skontaktować się z administratorem
w celu uzyskania pomocy.

Nie musisz nic robić, aby pozostać aktywnym członkiem listy mailingowej.

W razie jakichkolwiek pytań lub problemów można skontaktować się z właścicielem listy mailingowej
pod adresem

    $owner_email Prośba do listy mailingowej $listname

    $request

została odrzucona przez moderatora listy. Moderator podał
następujący powód odrzucenia:

„$reason”

Wszelkie pytania lub komentarze należy kierować do administratora listy
na adres:

    $owner_email Twoja wiadomość do listy mailingowej $listname została odrzucona z następujących
powodów:

$reasons

Oryginalna wiadomość otrzymana przez Mailman'a została załączona. Twoja subskrypcja została wyłączona na liście mailingowej $listname, ponieważ
otrzymała pewną liczbę zwrotów wskazujących, że może być problem z dostarczaniem
wiadomości na adres $sender_email. Możesz skontaktować się z administratorem poczty
w celu uzyskania pomocy.

W razie jakichkolwiek pytań lub problemów można skontaktować się z właścicielem listy mailingowej pod adresem

    $owner_email Witamy na liście mailingowej „$display_name”!

Aby wysłać wiadomość na tę listę, wyślij ją na adres:

  $listname

Możesz zrezygnować z subskrypcji lub dokonać zmian w opcjach 
za pośrednictwem poczty e-mail, wysyłając wiadomość na adres:

  $request_email

ze słowem „help” w temacie lub treści (bez cudzysłowów), 
a otrzymasz wiadomość zwrotną z instrukcjami.
Będziesz potrzebować hasła, do zmiany opcji, ale ze względów bezpieczeństwa
hasło to nie jest przekazywane w tej wiadomości. Jeśli zapomniałeś hasła,
musisz je zresetować za pośrednictwem interfejsu webowego użytkownika. nd. niedostępny readline niedostępne slice i range muszą być liczbami całkowitymi: ${value} {} 